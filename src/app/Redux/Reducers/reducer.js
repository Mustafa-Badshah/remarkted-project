"use client"
import { configureStore } from '@reduxjs/toolkit';
import userSlice from '../Slice/userSlice';

export const reducer = configureStore({

    reducer: {
        userSlice
    }
  });