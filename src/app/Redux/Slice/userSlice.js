import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"

const initialState = {
    userData: ["data"],
    loading: false,
    error: null,
    success: false
}




const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        addData: (state, action) =>{
            return "test data"
        }
    },
    

})

export const { addData } = usersSlice.actions
export default usersSlice.reducer