// "use client"
import { Navigate } from 'react-router';
import Header from '../component/Header';
// const DashboardDefault = Loadable(lazy(() => import('Views/Dashboard')));
import DashboardDefault from '../Views/Dashboard/page'

const MainRoutes = {
    path: '/',
    element: <Header />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        }
    ]
}

export default MainRoutes;
