// import { useRoutes } from 'react-router-dom';
import { useRouter } from 'next/router'

// routes
import MainRoutes from './MainRoutes';


// ==============================|| ROUTING RENDER ||============================== //

export default function ThemeRoutes() {
  const AuthenticatedRoutes = useRouter([MainRoutes]);
//   const otherRoutes = useRoutes([AuthenticationRoutes]);
    return AuthenticatedRoutes;
}
