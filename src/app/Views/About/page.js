"use client"
import { SfButton, SfIconCheck } from '@storefront-ui/react';
import BannerSection from "../../component/Banner";
import Image from "next/image";
import Link from 'next/link'
import '../../component/Component.css'
import BlogFour from '/public/accests/News-4.png';
import funIcons from '/public/accests/fun-icons.png';
import striveIcons from '/public/accests/strive-icons.png';
import careIcons from '/public/accests/care-icons.png';
import VisitStore from '../../component/VisitStore';
import RemarketedFamily from '../../component/RemarktedFamily';
import FeatureSingleProduct from '../../component/FeatureSingleProduct';



export default function About() {

    const bannerText = 'About us'

    return (
        <>

            <div className="container mt-[48px]">
                <div className=''>
                    <nav className={`--bs-breadcrumb-divider: '>';`} aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li className="breadcrumb-item bread-crumb-notactive"><Link href="/" className='bread-crumb-notactive'>Home</Link></li>
                            <li class="breadcrumb-item active" aria-current="page"> <Link href="/Views/About" className='bread-crumb-notactive'>About</Link></li>
                            {/* <li class="breadcrumb-item active" aria-current="page"><Link href="#" className='bread-crumb-isactive'>iPhone 13 Pro Max Gold</Link></li> */}
                        </ol>
                    </nav>
                </div>
            </div>
            <div className='container md:px-0 px-3 mt-[35px]'>
                <BannerSection dashboard={false} bannerText={bannerText} />
            </div>

            <div className="story-block lg:mt-[64px] lg:mb-[64px] my-[25px]">
                <div className="container md:px-0 px-3">
                    <div className='row items-center'>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12' >
                            <div className='social-text items-center'>
                                <h2 className='flex md:leading-[67.5px] justify-center font-semibold mt-2 md:mb-0 mb-4 text-[40px] lg:text-[64px] banner-header story-heading main-color'>Our Story</h2>

                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12' >
                            <div>
                                <p className="font-normal text-sm lg:text-base text-black">
                                    The 6.1-inch screen of the iPhone 13 Pro is equipped with Apple ProMotion, which makes scrolling or reading even more pleasant. The screen refreshes up to 120x per second when you are scrolling or gaming so that you always have the best screen experience. The iPhone 13 Pro now takes even sharper photos than its predecessors. The improved cameras and telephoto lens ensure that the photos are sharp, colorful and clear, even in lower light.
                                </p>

                                <p className="font-normal text-sm lg:text-base text-black ">
                                    Also new to the iPhone 13 Pro is the cinematic mode that allows you to easily switch objects or people during or even after filming. The iPhone 13 Pro is equipped with the new A15 Bionic Chip and 6 GB of RAM. This makes this iPhone faster and more powerful. The battery has also been upgraded and lasts 1.5 hours longer than that of the iPhone 12 Pro.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="mission-block bgmain-color pt-[50px] pb-[50px] lg:py-[125px] xs:py-[50px]">
                <div className="container">
                    <div className='row items-center'>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' >
                            <div className='block social-text items-center text-white'>
                                <h4 className="font-semibold text-[22px] lg:text-[32px] text-center mb-0">In every family a
                                    <br /> remarketed device.</h4>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' >
                            <h2 className='flex justify-center font-semibold mt-2 text-[40px] lg:text-[64px] text-white banner-header md:mb-0 mb-4'>Our mission</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div className="vision-block bg-white lg:pt-[64px] lg:pb-[64px] sm:py-[35px] pt-[35px] pb-[72px]">
                <div className="container md:px-0 px-3">
                    <div className="row items-center">
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' >
                            <h2 className='flex justify-center font-semibold mt-2 text-[40px] lg:text-[64px] text-black banner-header md:mb-0 mb-[32px]'>Our vision</h2>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' >
                            <div className="">
                                <h5 className="text-black font-semibold text-base md:text-[22px]">The circular economy is now.</h5>
                                <p className="font-normal text-sm lg:text-base text-black mt-[22px]">We are leading the way by giving mobile devices as long a life as possible and our customers as much security as possible</p>
                                <p className="font-normal text-sm lg:text-base text-black mt-[22px]">We help our community use devices in a way that enriches life</p>
                                <SfButton className='learn-more w-[192px]  h-[64px] border-radius-24 text-base lg:text-[22px] font-semibold mt-[10px] text-black hidden md:flex hover:bg-[#084566] hover:text-white hover:text-base hover:lg:text-[22px] hover:border-none' variant="secondary-800">Learn more</SfButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className='blog-images-block md:py-[64px] py-[40px]'>
                <div className="container">
                    <div className="row items-center">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 relative blog-image">
                            <div className='xl:pr-8 lg:pr-5 pr-0 md:pb-0 pb-3'>

                                <div className="fun-img relative">
                                {/* <Link href="/" > */}
                                    <Image
                                        src={BlogFour}
                                        alt="StoreImg"
                                        className="xl:h-[480px] lg:h-[400px] md:h-[340px] border-radius-24"
                                    />
                                    <div className='text-overlay pb-9 absolute px-11'></div>
                                {/* </Link> */}
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 blog-text'>
                            <div>
                                <Image 
                                    src={funIcons}
                                    className='h-[54px] w-[54px] lg:h-[70px] lg:w-[70px] xl:h-[130px] xl:w-[130px]'
                                />
                                <h5 className="text-black font-semibold text-base lg:text-[22px] mt-[16px] lg:mt-[20px]">Have fun together</h5>
                                <p className="font-normal text-sm lg:text-base text-black lg:mt-[28px] mt-3">We are leading the way by giving mobile devices as long a life as possible and our customers as much security as possible</p>
                            </div>
                        </div>
                    </div>

                    <div className="row items-center blog-img-margin">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 blog-text">
                            <div>
                                <Image 
                                    src={striveIcons}
                                    className='h-[54px] w-[54px] lg:h-[70px] lg:w-[70px] xl:h-[130px] xl:w-[130px]'
                                />
                                <h5 className="text-black font-semibold text-base lg:text-[22px] mt-[16px] lg:mt-[20px]">Have fun together</h5>
                                <p className="font-normal text-sm lg:text-base text-black lg:mt-[28px] mt-3">The quality of our devices and service are number one in our culture. This is why we strive for the highest level of service and knowledge in the field of refurbished iPhones.</p>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 relative blog-image'>
                            <div className='xl:pl-8 lg:pl-5 pl-0 md:pb-0 pb-3'>

                                <div className="fun-img relative">
                                    {/* <Link href="/" > */}
                                        <Image
                                            src={BlogFour}
                                            alt="StoreImg"
                                            className="xl:h-[480px] lg:h-[400px] md:h-[340px] border-radius-24"
                                        />
                                        <div className='text-overlay pb-9 absolute px-11'></div>
                                    {/* </Link> */}
                                    </div>
                            </div>
                            </div>
                    </div>

                    <div className="row items-center blog-img-margin">
                        <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 relative blog-image">
                            <div className='xl:pr-8 lg:pr-5 pr-0 md:pb-0 pb-3'>
                                <div className="fun-img relative">
                                {/* <Link href="/" > */}
                                    <Image
                                        src={BlogFour}
                                        alt="StoreImg"
                                        className="xl:h-[480px] lg:h-[400px] md:h-[340px] border-radius-24"
                                    />
                                    <div className='text-overlay pb-9 absolute px-11'></div>
                                {/* </Link> */}
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 blog-text'>
                            <div className=''>
                                <div>
                                    <Image 
                                        src={careIcons}
                                        className='h-[54px] w-[54px] lg:h-[70px] lg:w-[70px] xl:h-[130px] xl:w-[130px]'
                                    />
                                    <h5 className="text-black font-semibold text-base lg:text-[22px] mt-[16px] lg:mt-[20px]">Have fun together</h5>
                                    <p className="font-normal text-base text-black lg:mt-[28px] mt-3">Our customers and community can count on this:</p>
                                    <p className="flex points font-medium text-[#080808] text-sm lg:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px] text-white " /> <span>Your devices should always workas good as possible</span></p>
                                    <p  className="flex points  font-medium text-[#080808] text-sm lg:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px] text-white" /> <span>Our service is as sustainable as possible</span></p>
                                    <p className="flex points font-medium text-[#080808] text-sm lg:text-base"><SfIconCheck className="refurbished-b-check main-color mr-[12px] text-white" /> <span>If something does happen, we always fix it</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="md:pt-[64px] md:pb-[56px] pb-1 pt-[36px] bg-white visit-store about">
                <div className="container">
                    <VisitStore />
                </div>
            </div>

            <div className='main-visit-store'>
                <div className='container'>
                <RemarketedFamily />
                </div>
            </div>

            <div className='bg-white py-[20px] feature-product-section'>
                <div className='container'>
                    <FeatureSingleProduct />
                </div>
            </div>
        </>
    )
}