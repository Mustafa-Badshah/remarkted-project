"use client"
import { SfButton, SfIconCheck } from '@storefront-ui/react';
import Link from 'next/link'
import Image from "next/image";
import BlogOne from '/public/accests/News-1.png';
import BlogImg from '/public/accests/blog-avatar.png';
import BlogTwo from '/public/accests/News-2.png';
import iphone from '/public/accests/image2.png';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import '../../component/Component.css'


const Blog = () => {
    var products = [
        {
            name: 'iPhone 13 Pro 128GB Black',
            specs: ['128GB geheugen',
            'Twee jaar garantie',
            'Simlockvrij en inclusief'],
            id:1,
            imgSrc: iphone
        },
        {
            name: 'iPhone 13 Pro 128GB Black',
            specs: ['128GB geheugen',
            'Twee jaar garantie',
            'Simlockvrij en inclusief'],
            id:2,
            imgSrc: iphone
        },
        {
            name: 'iPhone 13 Pro 128GB Black',
            specs: ['128GB geheugen',
            'Twee jaar garantie',
            'Simlockvrij en inclusief'],
            id:3,
            imgSrc: iphone
        },
        {
            name: 'iPhone 13 Pro 128GB Black',
            specs: ['128GB geheugen',
            'Twee jaar garantie',
            'Simlockvrij en inclusief'],
            id:4,
            imgSrc: iphone
        },
        {
            name: 'iPhone 13 Pro 128GB Black',
            specs: ['128GB geheugen',
            'Twee jaar garantie',
            'Simlockvrij en inclusief'],
            id:5,
            imgSrc: iphone
        },
    ]

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        prevArrow: false,
        nextArrow: false
    };

    var mobileSliderContent = [
        {
            id: 1,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogTwo
        },
        {
            id: 2,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogTwo
        },
        {
            id: 3,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogTwo
        },
    ]

    return (
        <>
            <div>
                <div className="container mt-[48px]">
                    <div className=''>
                        <nav className={`--bs-breadcrumb-divider: '>';`} aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li className="breadcrumb-item bread-crumb-notactive"><Link href="/" className='bread-crumb-notactive'>Home</Link></li>
                                <li class="breadcrumb-item active" aria-current="page"> <Link href="#" className='bread-crumb-notactive'>Articles</Link></li>
                                <li class="breadcrumb-item active" aria-current="page"><Link href="#" className='bread-crumb-isactive'>Rigth to repair movement part 2/3</Link></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>

            <div className='blog-block pt-[25px]'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-xl-8 col-md-7 col-12 m-0'>
                            <div className='xl:pr-9 pr-3'>
                                <h2 className='main-color font-semibold text-[28px] leading-[34px] lg:text-[40px] lg:leading-[44px] xl:text-[54px] xl:leading-[70px]'>Right to repair movement part 2/3</h2>
                                <p className='text-black font-weight text-sm md:text-base mb-[54px] xl:mt-8 md:mt-5 md:mb-6'>April 16, 2024 - 4:30</p>
                                <div className='relative'>
                                    <Link href="/" >
                                        <Image
                                            src={BlogOne}
                                            alt="StoreImg"
                                            className="xlg:h-[480px] lg:h-auto w-100 border-radius-24"
                                        />

                                        <div className='single-text-overlay pb-9 absolute px-11'></div>
                                    </Link>

                                </div>
                                <div className='blog-des my-[40px]'>
                                    <h2 className='text-[22px] leading-[28px] font-semibold text-black'>Why is a Right To Repair Movement Needed?</h2>
                                    <div className='text-black font-normal text-sm lg:text-base mt-[22px]'>
                                        <p>Aside from manufacturer practices like battery reporting or the inaccessibility of OEM replacement parts, why is the Right To Repair movement important? Because by advocating for better product design and repairability, the movement addresses the critical issue of electronic waste.</p>
                                        <p>Aside from manufacturer practices like battery reporting or the inaccessibility of OEM replacement parts, why is the Right To Repair movement important? Because by advocating for better product design and repairability, the movement addresses the critical issue of electronic waste.</p>
                                        <p>Aside from manufacturer practices like battery reporting or the inaccessibility of OEM replacement parts, why is the Right To Repair movement important? Because by advocating for better product design and repairability, the movement addresses the critical issue of electronic waste.</p>
                                    </div>
                                </div>

                                <div className='hidden md:flex'>
                                    <div className='row bg-white border-radius-24 p-[30px] items-center' >
                                        <div className='col-xl-2 col-lg-2 col-md-3 col-sm-12 col-12'>
                                            <Image src={BlogImg} className='rounded-full' width={157} />
                                        </div>
                                        <div className='col-xl-10 col-lg-10 col-md-9 col-sm-12 col-12'>
                                            <h3 className='text-[22px] leading-[28px] font-semibold text-black' >by Sanne Wolters</h3>
                                            <p className='text-black font-normal md:text-md text-base mb-0'>Our resident expert on sustainability and right to repair, Sanne writes about how we van (re)use what’s already there. </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className='col-xl-4 col-md-5 col-12 hidden md:block'>
                            <div className='flex flex-col gap-3 lg:max-h-[unset] max-h-[1000px] overflow-y-auto'>
                                {
                                    products?.map(item => (
                                        <div className='w-100 bg-white shadow-sm p-[30px] rounded-3xl'>
                                            <div className='row justify-between lg:mb-4 mb-2'>
                                                <div className='col-lg-5'>
                                                    <div className='flex justify-center relative lg:mb-0 mb-3'>
                                                        <Image src={item?.imgSrc} className='rounded-md w-[100] max-w-[80px]' width={80} />
                                                        <span className='blog-count absolute top-[-10px] text-xl left-[-8px] flex items-center justify-center'>{item?.id}</span>
                                                    </div>
                                                </div>
                                                <div className='col-lg-7'>
                                                    <ul className='list-disc pl-2'>
                                                        {
                                                            item?.specs?.map((spec)=>(
                                                                <li className='lg:text-md text-sm font-medium text-black'>{spec}</li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className='flex gap-3 justify-between'>
                                                <h2 className='lg:text-[22px] lg:leading-[28px] text-[16px] leading-[22px] font-semibold text-black mb-0'>{item?.name}</h2>
                                                <SfButton className='bgcolor-sub1 w-[68px] h-[40px] xl:w-[88px] xl:h-[48px] m-font-femily main-color font-semibold xl:text-base border-radius-16 bgmain-hover-color text-sm' variant="secondary-800"> View</SfButton>
                                            </div>
                                        </div>
                                    ))
                                }
                            </div>
                        </div>
                        <div className='col-12 block md:hidden'>
                            <Slider {...settings}>
                                
                                {
                                    products?.map(item => (
                                        <div className='w-100 bg-white shadow-sm p-[30px] rounded-3xl'>
                                            <div className='row justify-between mb-4'>
                                                <div className='col-md-5 col-6'>
                                                    <div className='flex relative'>
                                                        <Image src={item?.imgSrc} className='rounded-md w-[100] sm:max-w-[80px] max-w-[62px] ml-6' width={80} />
                                                        <span className='blog-count absolute top-[-10px] text-xl left-[-8px] flex items-center justify-center'>{item?.id}</span>
                                                    </div>
                                                </div>
                                                <div className='col-lg-7 col-6'>
                                                    <ul className='list-disc pl-2'>
                                                        {
                                                            item?.specs?.map((spec)=>(
                                                                <li className='lg:text-md text-sm font-medium text-black'>{spec}</li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                            <div className='flex gap-3 justify-between items-center'>
                                                <h2 className='lg:text-[22px] lg:leading-[28px] text-[16px] leading-[22px] font-semibold text-black mb-0'>{item?.name}</h2>
                                                <SfButton className='bgcolor-sub1 w-[68px] h-[40px] xl:w-[88px] xl:h-[48px] m-font-femily main-color font-semibold xl:text-base border-radius-16 bgmain-hover-color text-sm' variant="secondary-800"> View</SfButton>
                                            </div>
                                        </div>
                                    ))
                                    }
                                
                            </Slider>
                        </div>
                    </div>
                </div>
            </div>

            <div className='py-[56px]'>
                <div className='container hidden md:block'>
                    <h2 className='text-[22px] leading-[28px] font-semibold text-black mb-[30px]'>
                        Related articles
                    </h2>
                    <div className='row'>
                        {mobileSliderContent?.map((data) =>(
                            <div className='col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 relative'>
                                <Image
                                    src={BlogTwo}
                                    alt="StoreImg"
                                    className="xl:h-[480px] h-auto w-100 border-radius-24"
                                />
                                <div className='text-overlay pb-9 absolute px-11'>
                                    <div>
                                        <p className='mb-6 text-white'>February 21, 2024</p>
                                        <h4 className='visit-heading font-semibold text-white text-[18px] leading-[24px] lg:text-[22px] lg:leading-[28px] lg:text-[26px] lg:leading-[30px] xl:text-[32px] xl:leading-[38px]'>5 cool iPhone facts you never knew</h4>
                                    </div>
                                </div>
                            </div>

                        ))}

                    </div>
                </div>

                <div className='container md:hidden '>
                    <div className='text-class shadow bg-white rounded-3xl blog-slider'>
                        <Slider {...settings}>
                            {mobileSliderContent?.map((data) => (
                                <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 relative'>
                                    <Image
                                        src={data?.img}
                                        alt="StoreImg"
                                        className="xl:h-[480px] h-100 w-100 border-radius-24"
                                    />
                                    <div className='text-overlay pb-9 absolute px-11'>
                                        <div>
                                            <h4 className=' mb-3 visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>{data?.title}</h4>
                                            <p className='text-white'>{data?.date}</p>
                                        </div>
                                    </div>
                                </div>
                            ))
                            }
                        </Slider>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Blog