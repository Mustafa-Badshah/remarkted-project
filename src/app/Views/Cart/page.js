"use client"
import { useState, useRef, useEffect } from 'react';
import { SfButton, SfIconAdd, SfIconRemove } from '@storefront-ui/react';
import { useCounter } from 'react-use';
import { useId, ChangeEvent } from 'react';
import { clamp } from '@storefront-ui/shared';
import logo from '/public/accests/RemarketedLogo.png';
import IphoneImg from '/public/accests/iphone-12.png';
import Product from '../../component/Porduct'
import OrderSummary from '../../component/Cart/orderSummary'
import { IoCartOutline } from "react-icons/io5";
import { SiTicktick } from "react-icons/si";
import { PiVan } from "react-icons/pi";
import { TbCash } from "react-icons/tb";
import Image from 'next/image';


const Cart = () => {
    const inputId = useId();
  const min = 1;
  const max = 10;
  const [value, { inc, dec, set }] = useCounter(min);
  function handleOnChange() {
    const { value: currentValue } = event.target;
    const nextValue = parseFloat(currentValue);
    set(clamp(nextValue, min, max));
  }

  let cartPorduct = [
    {
        productImage: '',
        productName: 'Iphone 12',
        productCondition: 'Excellent',
        productUnit: 1 ,
        productPrice: 45999,
        productTotalPrice: 45999,
    },
    {
        productImage: '',
        productName: 'Iphone 12',
        productCondition: 'Excellent',
        productUnit: 1,
        productPrice: 45999,
        productTotalPrice: 45999,
    },
    {
        productImage: '',
        productName: 'Iphone 12',
        productCondition: 'Excellent',
        productUnit: 1,
        productPrice: 45999,
        productTotalPrice: 45999,
    },
  ]

    return (
        <>
            <div className="cart-section py-[54px]">
                <div className="container">
                    <div className="row">
                        <div className='col-xl-8 col-lg-7 col-md-7 col-sm-12 col-12'>
                            <div className='pr-8'>
                                <div className='row'>
                                    <div className='col-md-4'>
                                        <h3 className='card-heading font-semibold text-[22px] leading-[28px]'>My Shopping Cart</h3>
                                    </div>
                                    <div className='col-md-8'>
                                        <div className='d-flex justify-between w-100 pl-4 progress-tabs'>
                                            <SfButton square="true" className='bg-white ' style={{borderRadius: '50%'}}>
                                                <span className='text-[#2FAC8E]'>
                                                    <IoCartOutline />
                                                </span>
                                            </SfButton>
                                            <SfButton square="true" className='bg-white ' style={{borderRadius: '50%'}}>
                                                <span className='text-[#CAE1D8]'>
                                                    <PiVan />
                                                </span>
                                            </SfButton>
                                            <SfButton square="true" className='bg-white ' style={{borderRadius: '50%'}}>
                                                <span className='text-[#CAE1D8]'>
                                                    <TbCash />
                                                </span>
                                            </SfButton>
                                            <SfButton square="true" className='bg-white ' style={{borderRadius: '50%'}}>
                                                <span className='text-[#CAE1D8]'>
                                                    <SiTicktick />
                                                </span>
                                            </SfButton>
                                        </div>
                                    </div>
                                </div>
                                <div className="cart-products-wrapper mt-10">
                                    <div className='row'>
                                        <div className='col-md-6'>
                                            <b className='font-bold'>Product</b>
                                        </div>
                                        <div className='col-md-2'>
                                            <b className='font-bold text-center block'>Units</b>
                                        </div>
                                        <div className='col-md-2'>
                                            <b className='font-bold text-center block'>Item Price</b>
                                        </div>
                                        <div className='col-md-2'>
                                            <b className='font-bold text-center block'>Sub Total</b>
                                        </div>
                                    </div>
                                    {
                                        cartPorduct?.map((data)=>(
                                        <div className='row items-center mt-2 p-border'>
                                                <div className='col-md-2 h-100'>
                                                    <div className='p-3 bg-white rounded-md shadow-sm w-100 max-w-[96px]'>
                                                        <Image src={IphoneImg} className="w-100 h-auto" />
                                                    </div>
                                                </div>
                                                <div className='col-md-4 h-100'>
                                                    <b className='font-semibold'>{data?.productName}</b>
                                                    <p className='mb-0'>Condition: {data?.productCondition}</p>
                                                    <p className='d-flex items-center gap-3 mb-0'>256GB <span className='bg-[#76B545] w-3 h-3 rounded-full'></span></p>
                                                </div>
                                                <div className='col-md-2 h-100'>
                                                    <div className='d-flex justify-center  items-center'>
                                                    <SfButton
                                                        variant="tertiary"
                                                        square
                                                        className='border-1 border-[#B6B6B6] pt-0 px-0 pb-0 rounded-none'
                                                        disabled={value <= min} 
                                                        aria-controls={inputId}
                                                        aria-label="Decrease value"
                                                        onClick={() => dec()}
                                                        >
                                                        <SfIconRemove style={{width: '14px', height: '14px'}} />
                                                        </SfButton>
                                                        <input
                                                        id={inputId}
                                                        type="number"
                                                        role="spinbutton"
                                                        className="appearance-none mx-2 w-8 text-center bg-transparent font-medium [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-inner-spin-button]:display-none [&::-webkit-inner-spin-button]:m-0 [&::-webkit-outer-spin-button]:display-none [&::-webkit-outer-spin-button]:m-0 [-moz-appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none disabled:placeholder-disabled-900 focus-visible:outline focus-visible:outline-offset focus-visible:rounded-sm p-0"
                                                        min={min}
                                                        max={max}
                                                        value={data?.productUnit}
                                                        onChange={handleOnChange}
                                                        />
                                                        <SfButton
                                                        variant="tertiary"
                                                        square
                                                        className="border-1 border-[#B6B6B6] pt-0 px-0 pb-0 rounded-none"
                                                        disabled={value >= max}
                                                        aria-controls={inputId}
                                                        aria-label="Increase value"
                                                        onClick={() => inc()}
                                                        >
                                                        <SfIconAdd style={{width: '14px', height: '14px', color: '#B6B6B6'}} />
                                                        </SfButton>
                                                    </div>
                                                </div>
                                                <div className='col-md-2 h-100'>
                                                    <p className='text-center'>€{data?.productPrice}</p>
                                                </div>
                                                <div className='col-md-2 h-100'>
                                                    <p className='text-center'>€{data?.productTotalPrice}</p>
                                                </div>
                                            </div>

                                        ))
                                    }
                                </div>
                                <div className='bgmain-color p-10 rounded-2xl mt-[32px]'>
                                    <div className='row items-center'>
                                        <div className='col-md-8'>
                                            <div className='d-flex items-end gap-3 mb-2'>
                                                <h2 className='text-xl text-white mb-0'>More extras? Join Remarketed Family.</h2>
                                                <Image src={logo} className="w-10 h-auto" />
                                            </div>
                                            <p className='text-white mb-0 font-light'>3-year warranty, always 5% discount, free goodies and more.</p>
                                        </div>
                                        <div className='col-md-4'>
                                            <SfButton className='bg-transparent w-fit h-[48px] text-white font-medium text-sm lg:text-lg border-radius-16 border-white border' variant="secondary-800"> Become member</SfButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-5'>
                            <div className='pl-3'>
                                <OrderSummary />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="main-product-div">
                <div className="container">
                    <h2 className='card-heading text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] font-semibold mb-4 lg:mb-4 text-center'>You might also like</h2>
                    <Product />
                </div>
            </div>
        </>

    )
}

export default Cart