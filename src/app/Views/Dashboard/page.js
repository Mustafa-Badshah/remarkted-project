import BannerSection from "../../component/Banner";
import RefurbishedPhones from "../../component/Refurbished";
import SocialHandling from "../../component/SocialHandling";
import Savings from '../../component/Savings'
import RemarktedFamily from '../../component/RemarktedFamily'
import RefurbishedBanner from "../../component/RefurbishedBanner";
import VisitStore from '../../component/VisitStore'
import OurCategory from '../../component/OurCategory'
import Blogs from '../../component/Blogs'

export default function Dashboard() {

  const bannerText = 'Enjoy more,pay less'

  return (
    <>
      {/* <Header /> */}
      <div className='container mt-[48px]'>
        <BannerSection dashboard={true} bannerText={bannerText}/>
      </div>
      {/* <div className='refurbished-div'>
        <div className='container'>
          <RefurbishedPhones />
        </div>
      </div> */}
      <div className="main-visit-store pb-0">
        <div className="container">
          <h2 className='typography-display-2 card-heading text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] font-semibold mb-3 lg:mb-4'>
            Explore Our Categories
          </h2>
          <OurCategory />
        </div>
      </div>
      <div className='main-visit-store'>
        <div className='container'>
           <RemarktedFamily />
        </div>
      </div>
      <div className="refurbihed-banner bgmain-color">
        <div className="container">
          <RefurbishedBanner />
        </div>
      </div>
      <div className="social-handling-section bg-white">
        <div className="container">
          <SocialHandling />
        </div>
      </div>
      <div className="savings-section">
        <div className="container">
          <Savings/>
        </div>
      </div>
      <div className="visit-store-section sm:bg-white">
        <div className="container">
          <VisitStore />
        </div>
      </div>
      <div className="blog-section">
        <div className="container">
          <h2 className='typography-display-2 card-heading text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] font-semibold mb-4'>
              Explore our blogs
          </h2>
          <Blogs />
        </div>
      </div>
      {/* <div className='main-product-div'>
        <div className='container'>
          <h2 className='typography-display-2 card-heading text-xl lg:text-3xl font-semibold'>
            Most popular refurbished phones
          </h2>
          <Product />
        </div>
      </div> */}
      {/* <Process /> */}
    </>
  );
}
