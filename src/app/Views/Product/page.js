"use client"
import { useEffect, useRef, useState } from 'react';
import Image from "next/image";
import Link from 'next/link'
import thumnailImageOne from '/public/accests/thumnail-img-1.png';
import mainImgOne from '/public/accests/m-img-1.png';
import preImgTwo from '/public/accests/pre-img2.png';
import '../../component/Component.css'
import Product from '../../component/Porduct';
import MobileDescription from '../../component/MobileDescription'
import RefurbishedBanner from '../../component/RefurbishedBanner';
import RefurbishedPhone from '../../component/RefurbishedPhone'
import FeatureSingleProduct from '../../component/FeatureSingleProduct';
import { useIntersection } from 'react-use';
import {
    SfIconStarHalf,
    SfIconLocalShipping,
    SfIconStarFilled,
    SfScrollable,
    SfButton,
    SfIconChevronLeft,
    SfIconChevronRight,
    SfIconInfo,
    SfRating
} from '@storefront-ui/react';
import classNames from 'classnames';


const images = [
    { imageSrc: mainImgOne, imageThumbSrc: thumnailImageOne, alt: 'backpack1' },
    { imageSrc: mainImgOne, imageThumbSrc: preImgTwo, alt: 'backpack2' },
    { imageSrc: mainImgOne, imageThumbSrc: thumnailImageOne, alt: 'backpack1' },
    { imageSrc: mainImgOne, imageThumbSrc: preImgTwo, alt: 'backpack2' },
    { imageSrc: mainImgOne, imageThumbSrc: thumnailImageOne, alt: 'backpack1' },
    { imageSrc: mainImgOne, imageThumbSrc: preImgTwo, alt: 'backpack2' },
    { imageSrc: mainImgOne, imageThumbSrc: thumnailImageOne, alt: 'backpack1' },
    { imageSrc: mainImgOne, imageThumbSrc: preImgTwo, alt: 'backpack2' },
];


const productView = () => {
    const lastThumbRef = useRef(null);
    const thumbsRef = useRef(null);
    const firstThumbRef = useRef(null);
    const [activeIndex, setActiveIndex] = useState(0);
    const [currency, setCurrency] = useState('€')

    const [windowSize, setWindowSize] = useState([
        window.innerHeight,
        window.innerWidth,
      ]);
    
      useEffect(() => {
        const windowSizeHandler = () => {
          setWindowSize([window.innerHeight, window.innerWidth]);
        };
        window.addEventListener("resize", windowSizeHandler);
    
        return () => {
          window.removeEventListener("resize", windowSizeHandler);
        };
      }, []);

    const firstThumbVisible = useIntersection(firstThumbRef, {
        root: thumbsRef.current,
        rootMargin: '0px',
        threshold: 1,
    });

    const lastThumbVisible = useIntersection(lastThumbRef, {
        root: thumbsRef.current,
        rootMargin: '0px',
        threshold: 1,
    });

    const onDragged = (event) => {
        if (event.swipeRight && activeIndex > 0) {
            setActiveIndex((currentActiveIndex) => currentActiveIndex - 1);
        } else if (event.swipeLeft && activeIndex < images.length - 1) {
            setActiveIndex((currentActiveIndex) => currentActiveIndex + 1);
        }
    };

    return (
        <>
            <div className='bg-white py-[48px]'>
                <div className='container md:px-0'>
                    <div className='mb-[35px]'>
                        <nav className={`--bs-breadcrumb-divider: '>';`} aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li className="breadcrumb-item bread-crumb-notactive"><Link href="/" className='bread-crumb-notactive'>Home</Link></li>
                                <li class="breadcrumb-item active" aria-current="page"> <Link href="/views/Product" className='bread-crumb-notactive'>Product</Link></li>
                                {/* <li class="breadcrumb-item active" aria-current="page"><Link href="#" className='bread-crumb-isactive'>iPhone 13 Pro Max Gold</Link></li> */}
                            </ol>
                        </nav>
                    </div>
                    <div className='row condition'>
                        <div className='col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='mobile-details mobile'>
                                <h4 className='H-details font-semibold text-[28px] leading-[34px] md:text-[40px] md:leading-[44px] xl:text-[54px] xl:leading-[60px]'>iPhone 13 Pro Max 128gb Gold</h4>
                            </div>
                            <div className="relative flex md:flex-row flex-col w-full max-h-424px md:max-h-[600px] aspect-[4/3] slider-wrapper">
                                <SfScrollable
                                    ref={thumbsRef}
                                    className="items-center w-full [&::-webkit-scrollbar]:hidden [-ms-overflow-style:none] [scrollbar-width:none]"
                                    direction={windowSize[1] < 768 ? "horizontal" : 'vertical'}
                                    activeIndex={activeIndex}
                                    prevDisabled={activeIndex === 0}
                                    nextDisabled={activeIndex === images.length - 1}
                                    slotPreviousButton={
                                        <SfButton
                                            className={classNames('absolute !rounded-full z-10 top-4 rotate-90 bg-white', {
                                                hidden: firstThumbVisible?.isIntersecting,
                                            })}
                                            variant="secondary"
                                            size="sm"
                                            square
                                            slotPrefix={<SfIconChevronLeft size="sm" />}
                                        />
                                    }
                                    slotNextButton={
                                        <SfButton
                                            className={classNames('absolute !rounded-full z-10 bottom-4 rotate-90 bg-white', {
                                                hidden: lastThumbVisible?.isIntersecting,
                                            })}
                                            variant="secondary"
                                            size="sm"
                                            square
                                            slotPrefix={<SfIconChevronRight size="sm" />}
                                        />
                                    }
                                >
                                    {images.map(({ imageThumbSrc, alt }, index, thumbsArray) => (
                                        <button
                                            // eslint-disable-next-line no-nested-ternary
                                            ref={index === thumbsArray.length - 1 ? lastThumbRef : index === 0 ? firstThumbRef : null}
                                            type="button"
                                            aria-label={alt}
                                            aria-current={activeIndex === index}
                                            key={`${alt}-${index}-thumbnail`}
                                            className={classNames(
                                                'md:w-[78px] md:h-auto relative shrink-0 pb-1 mx-4 -mb-2 border-b-4 snap-center cursor-pointer focus-visible:outline focus-visible:outline-offset slide-btn transition-colors flex-grow md:flex-grow-0',
                                                {
                                                    'border-primary-700': activeIndex === index,
                                                    'border-transparent': activeIndex !== index,
                                                },
                                            )}
                                            onMouseOver={() => setActiveIndex(index)}
                                            onFocus={() => setActiveIndex(index)}
                                        >
                                            <Image alt={alt} className="border border-neutral-200" width="78" height="78" src={imageThumbSrc} />
                                        </button>
                                    ))}
                                </SfScrollable>
                                <SfScrollable
                                    className="w-full h-full snap-x snap-mandatory [&::-webkit-scrollbar]:hidden [-ms-overflow-style:none] [scrollbar-width:none]"
                                    activeIndex={activeIndex}
                                    direction="horizontal"
                                    wrapperClassName="h-full m-auto"
                                    buttonsPlacement="none"
                                    isActiveIndexCentered
                                    drag={{ containerWidth: true }}
                                    onDragEnd={onDragged}
                                >
                                    {images.map(({ imageSrc, alt }, index) => (
                                        <div key={`${alt}-${index}`} className="flex justify-center h-full basis-full shrink-0 grow snap-center">
                                            <Image
                                                aria-hidden={activeIndex !== index}
                                                className="md:object-contain object-cover w-full h-full md:w-auto"
                                                alt={alt}
                                                src={imageSrc}
                                            />
                                        </div>
                                    ))}
                                </SfScrollable>
                            </div>
                        </div>

                        <div className='col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12'>
                            <div className='mobile-details'>
                                <h4 className='H-details font-semibold text-[28px] leading-[34px] lg:text-[54px] lg:leading-[60px]'>iPhone 13 Pro <br /> Max 128gb Gold</h4>
                            </div>
                            <div className='mode-block'>
                                <div className='color-block'>
                                    <h5 className='text-[#080808] font-semibold text-base lg:text-[22px] lg:leading-[28px]'>Color</h5>
                                    <div className='all-color-block'>
                                        <div className='color-verity active'>
                                            <div className='f-color bg-[#D4B976]'></div>
                                            <div className='color-des'>Gold</div>
                                        </div>
                                        <div className='color-verity'>
                                            <div className='f-color bg-[#54AAD8]'></div>
                                            <div className='color-des'>Blue</div>
                                        </div>
                                        <div className='color-verity'>
                                            <div className='f-color bg-[#333333]'></div>
                                            <div className='color-des'>Midnight</div>
                                        </div>
                                        <div className='color-verity'>
                                            <div className='f-color bg-[#BEC2C4]'></div>
                                            <div className='color-des'>Silver</div>
                                        </div>
                                        <div className='color-verity'>
                                            <div className='f-color bg-[#76B545]'></div>
                                            <div className='color-des'>Green</div>
                                        </div>
                                    </div>
                                </div>
                                <div className='memory-block'>
                                    <h5 className='text-[#080808] font-semibold text-base lg:text-[22px] lg:leading-[28px]'>Memory</h5>
                                    <div className='all-color-block'>
                                        <div className='color-verity'>
                                            <SfButton className='memory-btn text-white bgsubbtn-hover' variant="secondary-800">128 GB</SfButton>
                                        </div>
                                        <div className='color-verity'>
                                            <SfButton className='memory-btn-boder text-[#013049] hover:bg-[#084566] hover:text-white hover:text-base hover:lg:text-[22px] hover:border-none ' variant="secondary-800">256 GB</SfButton>
                                        </div>
                                        <div className='color-verity'>
                                            <SfButton className='memory-btn-boder text-[#013049] hover:bg-[#084566] hover:text-white hover:text-base hover:lg:text-[22px] hover:border-none ' variant="secondary-800" >512 GB</SfButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className='row mb-4 m-condition'>
                                <div className='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 '>
                                    <div className='condition-style flex items-center'>
                                        <h5 className='text-[#080808] font-semibold text-base lg:text-[22px] lg:leading-[28px] mb-0'>Condition</h5> 
                                        <SfIconInfo className='ml-2' /></div>
                                    </div>
                            </div>
                            <div className='row condition-row'>
                                <div className='col-xl-3 col-lg-3 col-md-4 col-sm-3 col-5'>
                                    <div className='p-rating'>
                                    <div><SfRating value={4.5} halfIncrement  /></div>
                                        <div className='font-semibold text-sm md:text-base mt-[5px]'>Very Good</div>
                                    </div>
                                </div>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12'>
                                    <div>
                                        <span className="text-[#080808] typography-text-lg text-sm ">new</span>
                                        <span className="text-[#080808] font-semibold text-sm" style={{ textDecoration: 'line-through' }}> 729,99  </span>
                                        <span className="block pb-1 text-[#013049] font-semibold text-base lg:text-[22px] lg:leading-[28px]">{currency} 487,50</span>
                                        <span className="block delivery-text color-sub2 mt-0 text-sm lg:text-base font-semibold"><SfIconLocalShipping /> Next day delivery</span>
                                    </div>
                                </div>
                                <div className='flex justify-center items-end col-xl-5 col-lg-5 col-md-4 col-sm-4 col-6'>
                                    <SfButton className='cart-btn border-radius-24 text-base lg:text-[22px] font-semibold bgmain-hover-color' variant="secondary-800">Add to cart</SfButton>
                                </div>
                            </div>
                            <div className='row m-condition condition-row'>
                                <div className='col-xl-3 col-lg-3 col-md-4 col-sm-3 col-5'>
                                    <div className='p-rating'>
                                        <div><SfRating value={4.5} halfIncrement  /></div>
                                        <div className='font-semibold text-sm md:text-base mt-[5px]'>Very Good</div>
                                    </div>
                                </div>
                                <div className='col-xl-4 col-lg-4 col-md-4 col-sm-5 col-12'>
                                    <div>
                                        <span className="text-[#080808] typography-text-lg text-sm ">new</span>
                                        <span className="text-[#080808] font-semibold text-sm text-base" style={{ textDecoration: 'line-through' }}> 729,99  </span>
                                        <span className="block pb-1  text-[#013049] font-semibold text-base lg:text-[22px] lg:leading-[28px]">{currency} 487,50</span>
                                        <span className="block delivery-text color-sub2 mt-0 text-sm lg:text-base font-semibold"><SfIconLocalShipping /> Next day delivery</span>
                                    </div>
                                </div>
                                <div className='flex justify-center col-xl-5 col-lg-5 col-md-4 col-sm-4 col-6 sm:mt-3 items-end'>
                                    <SfButton className='notify-btn border-radius-24 text-base lg:text-[22px] font-semibold hover:bg-[#084566] hover:text-white hover:text-base hover:lg:text-[22px] hover:border-none' variant="secondary-800">Notify me</SfButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Refurbished phones Div */}
            <div className="free-iphone">
                <RefurbishedPhone />
            </div>
            
            {/* Description */}
            <div className='bg-white py-[60px]'>
                <MobileDescription />
            </div>

            <div className='main-product-div'>
                <div className='container'>
                    <h2 className='card-heading text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] font-semibold mb-4 lg:mb-4'>
                    Most popular refurbished phones
                    </h2>
                    <Product />
                </div>
            </div>

            <div className="refurbihed-banner bgmain-color">
                <div className="container">
                    <RefurbishedBanner />
                </div>
            </div>

            <div className='bg-white py-[22px] hidden lg:block'>
                <div className='container'>
                    <FeatureSingleProduct />
                </div>
            </div>


        </>
    )
}

export default productView