"use client"
// import React from React;
import { ApolloProvider } from "@apollo/client"
import {useApollo} from "./Lib/apolloClient"
import { store } from './Redux/index'
import { Provider } from 'react-redux';

const App = ({ children }) =>{
    const apolloClient = useApollo(children.initialApolloState);

    return (
        <ApolloProvider   client={apolloClient}>
              {children}
        </ApolloProvider>
    )
    
}

export default App