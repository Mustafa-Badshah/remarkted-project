import { SfButton } from '@storefront-ui/react';
import Slider from "react-slick";
import Image from "next/image";
import SliderImg from '/public/accests/phone2.png';
import BannerImg from '/public/accests/phone1.png';

import './Component.css'

export default function BannerSection({ dashboard, bannerText}) {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        prevArrow: false,
        nextArrow: false
    };

    let movileViewCategory = [
        {
            id: 1,
            title: 'Enjoy more, pay less',
        },
        {
            id: 2,
            title: 'Super Duper Deal',
        },
        {
            id: 3,
            title: 'Super Duper Deal',
        },
    ]
    return (
        <>
        <div className='hidden md:block'>
            <div className="relative bgmain-color banner-div">
                <div className="md:flex md:flex-row-reverse md:justify-center xl:h-[416px] md:h-[350px] max-w-[1536px] mx-auto">
                    <div className="flex flex-col md:basis-2/5 md:items-stretch md:overflow-hidden">
                        <Image
                            src={BannerImg}
                            alt="Headphones"
                            className="h-full w-100 max-w-[400px]"
                            // width={400}
                        />
                    </div>
                    
                    {dashboard ?
                        <div className="p-4 md:p-10 md:flex md:flex-col md:justify-center md:items-start md:basis-2/4">
                            <h1 className="typography-display-2 md:typography-display-1 xl:leading-[60.5px] lg:leading-[55px] mt-2 mb-4 text-[40px] lg:text-[52px] xl:text-[54px] text-white font-semibold banner-header">
                                Enjoye more,
                                <br />
                                pay less
                                {/* </span> */}
                            </h1>

                            <div className="flex flex-col md:flex-row gap-4 mt-6">
                                <SfButton className='bgcolor-sub1 xl:w-[118px] xl:h-[64px] md:w-[98px] md:h-[54px] m-font-femily main-color font-semibold xl:text-xl text-lg xl:rounded-3xl rounded-xl bgmain-hover-color' variant="secondary-800"> View</SfButton>
                            </div>
                        </div>
                    : 
                    <div className="p-4 md:p-10 md:flex md:flex-col md:items-start md:basis-2/4">
                        <h1 className="typography-display-2 md:typography-display-1 xl:leading-[67.5px] lg:leading-[55px] lg:mt-[55px] mb-4 text-[40px] lg:text-[52px] xl:text-[64px] text-white font-semibold banner-header">
                        {bannerText}
                        </h1>
                    </div>
                    }
                </div>
            </div>
            {
                dashboard && 
                <div className='mt-9'>
                    <div className='row'>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className="relative min-h-[210px] bgmain-color banner-div">
                                <div className="md:flex md:flex-row-reverse md:justify-center min-h-[210px] max-w-[1536px] mx-auto">
                                    <div className="flex flex-col md:basis-2/5 md:items-stretch md:overflow-hidden">
                                        <Image
                                            src={BannerImg}
                                            alt="Headphones"
                                            className="banner-small-img"
                                            // width={140}
                                            // height={100}
                                        />
                                    </div>
                                    <div className="p-4 md:p-10 md:flex md:flex-col md:justify-center md:items-start md:basis-2/4">
                                        <h1 className=" mt-2 mb-4 text-[24px] lg:text-2xl xl:text-3xl text-white font-semibold banner-header">
                                            Super Duper Deal
                                        </h1>

                                        <div className="flex flex-col md:flex-row gap-4 mt-6">
                                            <SfButton className='bgcolor-sub1 w-[68px] h-[40px] xl:w-[88px] xl:h-[48px] m-font-femily main-color font-semibold xl:text-base border-radius-16 bgmain-hover-color text-sm' variant="secondary-800"> View</SfButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                            <div className="relative min-h-[210px] bgmain-color banner-div">
                                <div className="md:flex md:flex-row-reverse md:justify-center min-h-[210px] max-w-[1536px] mx-auto">
                                    <div className="flex flex-col md:basis-2/5 md:items-stretch md:overflow-hidden">
                                        <Image
                                            src={BannerImg}
                                            alt="Headphones"
                                            className="banner-small-img"
                                            // width={140}
                                            // height={100}
                                        />
                                    </div>
                                    <div className="p-4 md:p-10 md:flex md:flex-col md:justify-center md:items-start md:basis-2/4">
                                        <h1 className=" mt-2 mb-4 text-[24px] lg:text-2xl xl:text-3xl text-white font-semibold banner-header">
                                            Super Duper Deal
                                            {/* </span> */}
                                        </h1>

                                        <div className="flex flex-col md:flex-row gap-4 mt-6">
                                            <SfButton className='bgcolor-sub1 w-[68px] h-[40px] xl:w-[88px] xl:h-[48px] m-font-femily main-color font-semibold xl:text-base border-radius-16 bgmain-hover-color text-sm' variant="secondary-800"> View</SfButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
        {
            dashboard && 
            <div className='md:hidden mt-8'>
                <Slider {...settings}>
                    {movileViewCategory.map((data) => (
                        <div className="relative min-h-[320px] bgmain-color banner-div">
                            <div className="flex min-h-[320px] max-w-[1536px] mx-auto items-center">
                                <div className="p-4 md:p-10 md:flex md:flex-col md:justify-center items-start col-10 absolute left-[28px] top-[50%] translate-y-[-50%] z-10">
                                    <h1 className=" mt-2 mb-4 text-[40px] text-white font-semibold banner-header pr-10">
                                        {data.title}
                                    </h1>

                                    <div className="flex flex-col md:flex-row gap-4 mt-6">
                                        <SfButton className='bgcolor-sub1 m-font-femily main-color font-semibold xl:text-base border-radius-16 bgmain-hover-color text-sm w-[158px] height:[48px]' variant="secondary-800"> See the deals</SfButton>
                                    </div>
                                </div>
                                <div className="flex flex-col" style={{position: 'unset !important'}}>
                                    <Image
                                        src={SliderImg}
                                        alt="Headphones"
                                        className="banner-small-img"
                                        // width={140}
                                        // height={100}
                                    />
                                </div>
                            </div>
                        </div>

                    ))
                    }
                </Slider>
            </div>
        }
        {
            !dashboard && 
            <div className='md:hidden mt-8'>
                <div className="relative min-h-[320px] bgmain-color banner-div">
                    <div className="flex min-h-[320px] max-w-[1536px] mx-auto items-center">
                        <div className="mt-2 p-4 md:p-10 md:flex md:flex-col md:justify-center items-start col-10 absolute left-[28px] top-[0%] md:top-[50%] translate-y-0 md:translate-y-[-50%] z-10">
                            <h1 className=" mb-4 text-[40px] text-white font-semibold banner-header md:pr-10 pr-0">
                                {bannerText}
                            </h1>
                        </div>
                        <div className="flex flex-col" style={{position: 'unset !important'}}>
                            <Image
                                src={SliderImg}
                                alt="Headphones"
                                className="banner-small-img about"
                                // width={140}
                                // height={100}
                            />
                        </div>
                    </div>
                </div>
            </div>
        }
        </>
    );
}
