import React from 'react';
import Image from "next/image";
import { SfButton } from '@storefront-ui/react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Link from 'next/link'
import BlogOne from '/public/accests/News-1.png';
import BlogTwo from '/public/accests/News-2.png';
import BlogThree from '/public/accests/News-3.png';
import BlogFour from '/public/accests/News-4.png';


const Blogs = () => {

    const blogUrl = '/Views/Blog'
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        prevArrow: false,
        nextArrow: false
    };

    var mobileSliderContent = [
        {
            id: 1,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogOne
        },
        {
            id: 2,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogTwo
        },
        {
            id: 3,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogThree
        },
        {
            id: 4,
            date: 'February 21, 2024',
            title: '5 cool iPhone facts you never knew',
            img: BlogFour
        },
    ]

    return (
        <>
            <div className='hidden md:block'>
                <div className='row hidden lg:flex'>
                    <div className='col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12 relative'>
                        <Link href={blogUrl} >
                            <Image
                                src={BlogOne}
                                alt="StoreImg"
                                className="xlg:h-[480px] lg:h-[440px] md:h-[260px] border-radius-24"
                            />

                            <div className='text-overlay pb-9 absolute px-11'>
                                <div>
                                    <p className='mb-6 text-white'>February 21, 2024</p>
                                    <h4 className='visit-heading font-semibold text-white text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>5 cool iPhone facts you never knew</h4>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 relative'>
                        <Link href={blogUrl} >
                            <Image
                                src={BlogTwo}
                                alt="StoreImg"
                                className="xlg:h-[480px] lg:h-[440px] md:h-[260px] border-radius-24"
                            />
                            <div className='text-overlay pb-9 absolute px-11'>
                                <div>
                                    <p className='mb-6 text-white'>February 21, 2024</p>
                                    <h4 className='visit-heading font-semibold text-white text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>5 cool iPhone facts you never knew</h4>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
                <div className='row mt-4 hidden lg:flex'>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 relative'>
                        <Link href={blogUrl} >
                            <Image
                                src={BlogThree}
                                alt="StoreImg"
                                className="xlg:h-[480px] lg:h-[440px] md:h-[260px] border-radius-24"
                            />
                            <div className='text-overlay pb-9 absolute px-11'>
                                <div>
                                    <p className='mb-6 text-white'>February 21, 2024</p>
                                    <h4 className='visit-heading font-semibold text-white text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>5 cool iPhone facts you never knew</h4>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 relative'>
                        <Link href={blogUrl} >
                            <Image
                                src={BlogFour}
                                alt="StoreImg"
                                className="xlg:h-[480px] lg:h-[440px] md:h-[260px] border-radius-24"
                            />
                            <div className='text-overlay pb-9 absolute px-11'>
                                <div>
                                    <p className='mb-6 text-white'>February 21, 2024</p>
                                    <h4 className='visit-heading font-semibold text-white text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>5 cool iPhone facts you never knew</h4>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
            <div className='md:hidden text-class shadow bg-white rounded-3xl blog-slider'>
                <Slider {...settings}>
                    {mobileSliderContent.map((data) => (
                        <div className='col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12 relative'>
                            <Link href={blogUrl} >
                                <Image
                                    src={data.img}
                                    alt="StoreImg"
                                    className="xlg:h-[480px] lg:h-[440px] md:h-[260px] border-radius-24"
                                />
                                <div className='text-overlay pb-9 absolute px-11'>
                                    <div>
                                        <h4 className=' mb-3 visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>{data.title}</h4>
                                        <p className='text-white'>{data.date}</p>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    ))
                    }
                </Slider>
            </div>
        </>
    )
}

export default Blogs