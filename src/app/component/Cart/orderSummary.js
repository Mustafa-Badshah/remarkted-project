import { useState, useRef, useEffect } from 'react';
import { SfButton, SfIconCheck, SfIconCheckCircle, SfIconClose, SfInput, SfLink, SfIconArrowForward } from '@storefront-ui/react';

const orderDetails = {
  items: 3,
  originalPrice: 7824.97,
  savings: -787.0,
  delivery: 0.0,
  tax: 457.47,
};

export default function OrderSummary() {
  const errorTimer = useRef(0);
  const positiveTimer = useRef(0);
  const informationTimer = useRef(0);
  const [inputValue, setInputValue] = useState('');
  const [promoCode, setPromoCode] = useState(0);
  const [informationAlert, setInformationAlert] = useState(false);
  const [positiveAlert, setPositiveAlert] = useState(false);
  const [errorAlert, setErrorAlert] = useState(false);

  useEffect(() => {
    clearTimeout(errorTimer.current);
    errorTimer.current = window.setTimeout(() => setErrorAlert(false), 5000);
    return () => {
      clearTimeout(errorTimer.current);
    };
  }, [errorAlert]);

  useEffect(() => {
    clearTimeout(positiveTimer.current);
    positiveTimer.current = window.setTimeout(() => setPositiveAlert(false), 5000);
    return () => {
      clearTimeout(positiveTimer.current);
    };
  }, [positiveAlert]);

  useEffect(() => {
    clearTimeout(informationTimer.current);
    informationTimer.current = window.setTimeout(() => setInformationAlert(false), 5000);
    return () => {
      clearTimeout(informationTimer.current);
    };
  }, [informationAlert]);

  const formatPrice = (price) =>
    new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(price);

  const itemsSubtotal = () =>
    orderDetails.originalPrice + orderDetails.savings + orderDetails.delivery + orderDetails.tax;

  const totalPrice = () => itemsSubtotal() + promoCode;

  const checkPromoCode = (event) => {
    event.preventDefault();
    if ((promoCode === -100 && inputValue.toUpperCase() === 'VSF2020') || !inputValue) return;
    if (inputValue.toUpperCase() === 'VSF2020') {
      setPromoCode(-100);
      setPositiveAlert(true);
    } else {
      setErrorAlert(true);
    }
  };

  const removePromoCode = () => {
    setPromoCode(0);
    setInformationAlert(true);
  };

  return (
    <div>
      <div className="md:shadow-lg md:rounded-3xl main-light-color py-[38px] px-[30px]">
        <div className="pb-4">
          <p className="font-semibold text-3xl mb-0 text-white">Summary</p>
        </div>
        <div className="pb-2">
          <div className="flex justify-between typography-text-base text-white">
            <div className="">
              <p className='font-semibold mb-0'>Sub Total</p>
            </div>
            <div className="flex flex-col text-right">
              <p className='mb-0'>{formatPrice(itemsSubtotal())}</p>
            </div>
          </div>
          {promoCode ? (
            <div className="flex items-center mb-5 py-5 border-b border-neutral-200">
              <p>PromoCode</p>
              <SfButton size="sm" variant="tertiary" className="ml-auto mr-2" onClick={removePromoCode}>
                Remove
              </SfButton>
              <p>{formatPrice(promoCode)}</p>
            </div>
          ) : (
            <form className="flex gap-x-2 pt-4 pb-[30px] border-b border-neutral-300 mb-4" onSubmit={checkPromoCode}>
              <SfInput
                value={inputValue}
                placeholder="Discount Code"
                wrapperClassName="oulined-input-wrapper bg-transparent grow"
                className='oulined-input'
                onChange={(event) => setInputValue(event.target.value)}
              />
              <button type="submit" className='icon-btn-solid font-semibold text-[30px] w-12 flex justify-center items-center text-[#013049] bgmain-hover-color px-0' variant="secondary">
                <SfIconArrowForward className='text-[#013049]' />
                {/* <FiArrowRight className='text-[#013049]'/> */}
              </button>
            </form>
          )}
          <div className="flex justify-between pb-4 text-white">
            <p className='font-semibold'>Total</p>
            <p>{formatPrice(totalPrice())}</p>
          </div>
          <SfButton size="lg" className="w-full visit-store-btn font-semibold text-xl  bgmain-hover-color">
            <span className='text-[#013049]'>Proceed to payment</span>
          </SfButton>
          <div className='flex flex-col items-center'>
            <div className="banner-block mt-5 mb-0">
              <p className="flex points font-medium text-white text-sm xl:text-base mb-[12px]"><SfIconCheck className="refurbished-b-check mr-[12px] " /><span> At least 2 years warranty</span></p>
              <p  className="flex points  font-medium text-white text-sm xl:text-base mb-[12px]"><SfIconCheck className="refurbished-b-check mr-[12px]" /><span> 30 days trial period</span></p>
              <p className="flex points font-medium text-white text-sm xl:text-base mb-[12px]"><SfIconCheck className="refurbished-b-check mr-[12px] " /><span> Free shipping and return</span></p>
            </div>
          </div>
        </div>
      </div>
      <div className="absolute top-0 right-0 mx-2 mt-2 sm:mr-6">
        {positiveAlert && (
          <div
            role="alert"
            className="flex items-start md:items-center shadow-md max-w-[600px] bg-positive-100 pr-2 pl-4 mb-2 ring-1 ring-positive-200 typography-text-sm md:typography-text-base py-1 rounded-md"
          >
            <SfIconCheckCircle className="mr-2 my-2 text-positive-700" />
            <p className="py-2 mr-2">Your promo code has been added.</p>
            <button
              type="button"
              className="p-1.5 md:p-2 ml-auto rounded-md text-positive-700 hover:bg-positive-200 active:bg-positive-300 hover:text-positive-800 active:text-positive-900"
              aria-label="Close positive alert"
              onClick={() => setPositiveAlert(false)}
            >
              <SfIconClose className="hidden md:block" />
              <SfIconClose size="sm" className="md:hidden block" />
            </button>
          </div>
        )}
        {informationAlert && (
          <div
            role="alert"
            className="flex items-start md:items-center shadow-md max-w-[600px] bg-positive-100 pr-2 pl-4 mb-2 ring-1 ring-positive-200 typography-text-sm md:typography-text-base py-1 rounded-md"
          >
            <SfIconCheckCircle className="mr-2 my-2 text-positive-700" />
            <p className="py-2 mr-2">Your promo code has been removed.</p>
            <button
              type="button"
              className="p-1.5 md:p-2 ml-auto rounded-md text-positive-700 hover:bg-positive-200 active:bg-positive-300 hover:text-positive-800 active:text-positive-900"
              aria-label="Close positive alert"
              onClick={() => setInformationAlert(false)}
            >
              <SfIconClose className="hidden md:block" />
              <SfIconClose size="sm" className="md:hidden block" />
            </button>
          </div>
        )}
        {errorAlert && (
          <div
            role="alert"
            className="flex items-start md:items-center max-w-[600px] shadow-md bg-negative-100 pr-2 pl-4 ring-1 ring-negative-300 typography-text-sm md:typography-text-base py-1 rounded-md"
          >
            <p className="py-2 mr-2">This promo code is not valid.</p>
            <button
              type="button"
              className="p-1.5 md:p-2 ml-auto rounded-md text-negative-700 hover:bg-negative-200 active:bg-negative-300 hover:text-negative-800 active:text-negative-900"
              aria-label="Close error alert"
              onClick={() => setErrorAlert(false)}
            >
              <SfIconClose className="hidden md:block" />
              <SfIconClose size="sm" className="md:hidden block" />
            </button>
          </div>
        )}
      </div>
    </div>
  );
}
