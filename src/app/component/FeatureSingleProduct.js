import React from 'react'
import Image from "next/image";
import { SfButton, SfIconStarFilled, SfIconStarHalf, SfIconLocalShipping } from '@storefront-ui/react'
import ProductImage from '/public/accests/image2.png';
import './Component.css'


const featureSingleProduct = () => {
    return (
        <>
            <div className='row'>
                <div className='col-xl-5 col-lg-4 lg:block hidden'>
                    <div className="flex  gap-4 lg:gap-6  w-full  lg:max-w-[350px] max-h-[300] bg-white border-radius-16 m-refurbished-card lg:justify-start justify-center">
                        <div className=" flex flex-row items-center">
                            <Image src={ProductImage} className='refurbished-img' />
                            {/* <div> */}
                            <p className='refurbished-text mt-1 font-semibold mx-3 text-black mb-0'> iPhone 13 Pro 128GB Black</p>
                            {/* </div> */}
                            <div className='sub-percent font-semibold text-sm text-white'>
                                -25%
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col-xl-7 col-lg-8 '>
                    <div className='m-single-details'>
                        <div className='p-rating'>
                            <div><SfIconStarFilled className='rating-color' /> <SfIconStarFilled className='rating-color' /> <SfIconStarFilled className='rating-color' /> <SfIconStarFilled className='rating-color' /> <SfIconStarHalf className='rating-color' /></div>
                            <div className='font-semibold text-sm md:text-base mt-[5px]'>Very Good</div>
                        </div>
                        <div className='d-flex p-info items-center'>
                            <p className="block delivery-text color-sub2 text-sm lg:text-base font-semibold m-0">
                            <SfIconLocalShipping /> Next day delivery
                            </p>

                            <div>
                                <span className=" typography-text-lg text-[13px] md:text-base ">new</span>
                                <span className=" typography-text-lg text-[13px] md:text-base inline-block ml-1" style={{ textDecoration: 'line-through' }}>$729,99  </span>
                                <span className="block font-bold price-font text-right">$669,99</span>
                            </div>
                            <div>
                                <SfButton size="md" className='bgcolor-sub1 m-font-femily h-[48px]  text-black text-sm md:text-base font-semibold border-radius-16 bgmain-hover-color' variant="secondary-800">
                                    Add to cart
                                </SfButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default featureSingleProduct