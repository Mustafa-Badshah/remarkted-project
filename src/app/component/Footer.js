import { useState } from 'react';
import {
    SfButton,
    SfLink,
    SfAccordionItem,
    SfIconChevronLeft,
    SfIconCall,
    SfIconEmail
  } from '@storefront-ui/react';
  import Image from "next/image";
  import ContactImg from '/public/accests/contact.png';
  import classNames from 'classnames';

  import './Component.css'
  
  
  export default function Footer() {

    const [opened, setOpened] = useState([]);
    const isOpen = (id) => opened.includes(id);

    const handleToggle = (id) => (open) => {
        if (open) {
            setOpened((current) => [...current, id]);
        } else {
            setOpened((current) => current.filter((item) => item !== id));
        }
    };


    const remarketed = () =>{
      return (
          <div className='f-col-subheading mt-4'>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            About Remarketed
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Our Process
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Community (become a member)
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Our mission and vision
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Sustainability
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            What is refurbishment
            </SfLink>
            <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Blog
            </SfLink>
        </div>
      )
    }

    const services = () =>{
      return (
        <div className='f-col-subheading mt-4'>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Customer Service
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Track & Trace
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Repair
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Buy Back
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Store
          </SfLink>
        </div>
      )
    }

    const refurbished = () =>{
      return (
        <div className='f-col-subheading mt-4'>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone 13
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone 12 Pro
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone 12
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone 11
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone SE 2020
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Refurbished iPhone X
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            All refurbished iPhones
          </SfLink>
          <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
              Refurbished iPhone SE 2020
          </SfLink>
        </div>
      )
    }

    const support = () =>{
      return (
        <div className='f-col-subheading mt-4'>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Returns
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Guarantee
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Complaints
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Privacy
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Terms & Conditions
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
          General terms and conditions of sale
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
          B2B
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            FAQ
        </SfLink>
        <SfLink href="#" variant="secondary"  className="no-underline m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base ">
            Disclaimer
        </SfLink>
      </div>
      )
    }
    const accordionItems = [
      {
          id: 'acc-1',
          summary: 'Remarketed',
          details: remarketed(),
      },
      {
          id: 'acc-2',
          summary: 'Service',
          details: services(),
      },
      {
        id: 'acc-3',
        summary: 'Refurbished iPhones',
        details: refurbished(),
    },
    {
        id: 'acc-4',
        summary: 'Support',
        details: support(),
    },

  ];


    return (
      <>
        <div className='pt-[20px]  main-light-color  '>
          <div className='container '>
            <div className='row'>
            <div className='col-xl-2 col-lg-2 col-md-2 hidden lg:flex'>
              <div className='contact-img'>
                <Image src={ContactImg} className='rounded-full' width={157} />
              </div>
            </div>
            <div className='col-xl-4 col-lg-4 col-md-4'>
              <div className='contact-div mt-3'>
                <h3 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Contact</h3>
                <p className='m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base'>Do you have a question about one of our products or services? Feel free to email or call us. We are happy to help you as quickly as possible.</p>
              </div>
              <hr className='mt-5 mb-3 line-style md:hidden ' />
            </div>
            <div className='col-xl-3 col-lg-3 col-md-4 '>
            <div className='contact-div mt-3'>
            <h3 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Call us</h3>
                <p className='m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base'>Monday to Friday from 10:00 am to 4:00 pm.</p>
                <SfButton className='contact-btn main-color border-radius-16 mt-2 text-sm xl:text-base text-white' variant="secondary-800">
                  <SfIconCall /> +31 85 0043 960
                </SfButton>
              </div>
            </div>
            <div className='col-xl-3 col-lg-3 col-md-4 '>
            <div className='contact-div mt-3 mb-4'>
            <h3 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Mail us</h3>
                <p className='m-font-femily block mb-2.5 text-white font-normal text-sm xl:text-base'>A substantive response within 24 hours.</p>
                <SfButton className='contact-btn main-color border-radius-16 mt-2 text-sm xl:text-base text-white' variant="secondary-800">
                  <SfIconEmail />  Customer Service
                </SfButton>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div className='footer-div mb-0'>
          <div className='container main-color'>
            <footer className="bgmain-color">
              <div className='row'>
                <div className='col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 hidden lg:flex'>
                  <div className='div-block'>
                    <h4 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Remarketed</h4>
                    {remarketed()}
                  </div>
                </div>
                <div className='col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 hidden lg:flex'>
                  <div className='div-block'>
                    <h4 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Service</h4>
                    {services()}
                  </div>
                </div>
                <div className='col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 hidden lg:flex'>
                  <div className='div-block'>
                    <h4 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Refurbished iPhones</h4>
                    {refurbished()}
                  </div>
                </div>
                <div className='col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 hidden lg:flex'>
                  <div className='div-block'>
                    <h4 className='m-font-femily text-white text-base xl:text-[22px] font-semibold'>Support</h4>
                    {support()}
                  </div>
                </div>
                
              </div>

              <div className=' lg:hidden'>
              <div className=" rounded-md divide-y text-neutral-900">
                  {accordionItems.map(({ id, summary, details }) => (
                      <SfAccordionItem
                      key={id}
                      summary={
                        <div className="flex justify-between mt-4 mb-4 font-medium  active:neutral-100 m-font-femily text-white text-base xl:text-[22px] font-semibold">
                          <p>{summary}</p>
                          <SfIconChevronLeft
                            className={classNames('text-neutral-500 text-base text-white', {
                              'rotate-90': isOpen(id),
                              '-rotate-90': !isOpen(id),
                            })}
                          />
                        </div>
                      }
                      onToggle={handleToggle(id)}
                      open={isOpen(id)}
                    >
                          {details}

                      </SfAccordionItem>
                  ))}
                </div>
              </div>
  
              <div className='reserved-div font-normal text-base text-[#ffffff80] my-[30px] '>
                    <p>All rights reserved. Apple, iPhone, iPad are brand names of Apple. © Copyright 2024 Remarketed.nl</p>
              </div>
              <hr />
  
  
            </footer>
          </div>
        </div>
      </>
    );
  }
  