"use client"
import { useRef, useState } from 'react';
import {
  SfButton,
  SfIconShoppingCart,
  SfIconCheck,
  SfIconPerson,
  SfIconSearch,
  SfIconMenu,
  SfLink,
  useDisclosure,
  useTrapFocus,
  SfDrawer,
  SfIconClose,
  SfListItem,
} from '@storefront-ui/react';
import { useClickAway } from 'react-use';
import Link from 'next/link'
import Image from "next/image";
import Logo from '/public/accests/Logo.png';
import { CSSTransition } from 'react-transition-group';

export default function Header() {
  const { close, toggle, isOpen } = useDisclosure();
  const drawerRef = useRef(null);
  const menuRef = useRef(null);


  useTrapFocus(drawerRef, {
    activeState: isOpen,
    arrowKeysUpDown: true,
    initialFocus: 'container',
  });
  useClickAway(menuRef, () => {
    close();
  });


  const headerPagesLink = [
    {
      name: 'About Us',
      url: '/Views/About'
    },
    {
      name: 'Service',
      url: ''
    },
    {
      name: 'Contact',
      url: ''
    },
  ]
  const headerCategoryLink = [
    {
      name: 'Deals',
      url: ''
    },
    {
      name: 'iPhone',
      url: ''
    },
    {
      name: 'Mackbook',
      url: ''
    },
    {
      name: 'iPad',
      url: ''
    },
    {
      name: 'Sell',
      url: ''
    },
    {
      name: 'Store',
      url: ''
    }
  ]

  return (
    <>
      <div className='bg-[#2FAC8E] lg:flex top-head'>
        <div className='container p-1'>
          <div className='row'>
            <div className='col-md-4'>
              <div className='flex items-center gap-2.5'><SfIconCheck className='header-check' /><p className='h-top-heading font-semibold text-sm 2xl:text-base '> Ordered before 23:59 = delivered tomorrow</p></div>
            </div>
            <div className='col-md-4'>
              <div className='flex items-center gap-2.5'><SfIconCheck className='header-check' /><p className='h-top-heading font-semibold text-sm 2xl:text-base '> 2 year warranty (3 years for members)</p></div>
            </div>
            <div className='col-md-2'>
              <div className='flex items-center gap-2.5'><SfIconCheck className='header-check' /><p className='h-top-heading font-semibold text-sm 2xl:text-base '> Fee shipping</p></div>
            </div>
            <div className='col-md-2'>
              <div className='flex items-center gap-2.5'><SfIconCheck className='header-check' /><p className='h-top-heading font-semibold text-sm 2xl:text-base '> Sustainable choice</p></div>
            </div>
          </div>
        </div>
      </div>
      <div className='h-[80px] bgmain-color'>
        <div className='container h-[80px] flex'>
          <header className="flex justify-center w-full  text-white border-0 border-neutral-200 ">
            <div className="flex flex-wrap header-block w-full justify-between">
              <div className='d-flex justify-between gap-2 nav-items-wrapper'>
                <Link
                  href="/"
                  aria-label="SF Homepage"
                  className="inline-block mr-4 focus-visible:outline focus-visible:outline-offset focus-visible:rounded-sm shrink-0"
                >
                  <Image
                    src={Logo}
                    alt="Sf Logo"
                    className="h-[20px] w-auto"
                  />
                </Link>
                <nav className='sm:hidden md:flex sm:flex'>
                  <div className="sm:space-x-3 md:space-x-8">
                    {headerCategoryLink.map((data) => (
                      <Link className='menu-style  sm:text-base text-2*2 hover:text-white' href={data?.url}>
                        {data?.name}
                      </Link>
                    ))}
                    {headerPagesLink.map((data) => (
                      <Link className='menu-style pages sm:text-base text-2*2 hover:text-white' href={data?.url}>
                        {data?.name}
                      </Link>
                    ))}
                  </div>
                </nav>
              </div>

              <div>
              <nav className="flex-1 flex justify-end lg:order-last lg:ml-4">
                <div className="flex flex-row flex-nowrap lg:gap-1 2xl:gap-3">
                  <SfButton
                    className="btn-account text-white hidden xs:flex "
                    aria-label="Account"
                    variant="tertiary"
                    square
                    slotPrefix={<SfIconPerson />}
                  >
                    <p className="xl:inline-flex whitespace-nowrap m-0">Account</p>
                  </SfButton>
                  <SfButton
                    className="btn-search text-white hidden xs:flex "
                    aria-label="Account"
                    variant="tertiary"
                    square
                    slotPrefix={<SfIconSearch />}
                  >
                  </SfButton>
                  <Link href='/Views/Cart'>
                    <SfButton
                      className="btn-cart text-white"
                      aria-label="Account"
                      variant="tertiary"
                      square
                      slotPrefix={<SfIconShoppingCart />}
                    >
                      <span className='cart-count font-bold'>2</span>
                    </SfButton>
                  </Link>
                  <SfButton
                    aria-label="Open categories"
                    className="xl:hidden ml-2 text-white hover:text-white active:text-white hover:bg-primary-800 active:bg-primary-900"
                    square
                    variant="tertiary"
                    onClick={toggle}
                  >
                    <SfIconMenu />
                  </SfButton>
                </div>
              </nav>
              </div>      
              { isOpen && 
                // <nav className='z-10'>
                // <ul>
                //   <li role="none">
                    <CSSTransition
                      in={isOpen}
                      timeout={500}
                      unmountOnExit
                      classNames={{
                        enter: '-translate-x-full md:opacity-0 md:translate-x-0',
                        enterActive: 'translate-x-0 md:opacity-100 transition duration-500 ease-in-out',
                        exitActive: '-translate-x-full md:opacity-0 md:translate-x-0 transition duration-500 ease-in-out',
                      }}
                    >
                      <SfDrawer
                        ref={drawerRef}
                        open
                        disableClickAway
                        placement="right"
                        className="flex flex-wrap justify-center items-center header-drop-down z-10 bg-white fixed h-screen overflow-y-auto top-0 lg:w-[340px] w-[240px] right-0"
                      >
                        <div className='flex flex-col items-center w-100 flex-nowrap'>
                          {headerPagesLink.map(( data ) => (
                            <div key={data?.name} className="pt-3 drop-down-links bg-white">
                                <Link
                                      // as="a"
                                      // size="sm"
                                      role="none"
                                      href={data?.url}
                                      className="  header-side-text font-semibold text-base"
                                    >
                                      {data?.name}
                                    </Link>
                              {/* <hr className='text-[#013049] mb-0'/> */}
                              {/* </h2> */}
                            </div>
                          ))}
                          {headerCategoryLink.map(( data ) => (
                            <div key={data?.name} className="pt-3 drop-down-links bg-white">
                                <Link
                                      // as="a"
                                      // size="sm"
                                      role="none"
                                      href={data?.url}
                                      className="  header-side-text font-semibold text-base"
                                    >
                                      {data?.name}
                                    </Link>
                              {/* <hr className='text-[#013049] mb-0'/> */}
                              {/* </h2> */}
                            </div>
                          ))}
                        </div>
                        
                      
                        <SfButton
                          square
                          size="sm"
                          variant="tertiary"
                          aria-label="Close navigation menu"
                          onClick={close}
                          className="absolute right-6 top-6 hover:bg-white active:bg-white"
                        >
                          <SfIconClose className="text-neutral-500 text-3xl" />
                        </SfButton>
                      </SfDrawer>
                    </CSSTransition>
              //     </li>
              //   </ul>
              // </nav>
              }
            </div>
          </header>
        </div>
      </div>
    </>
  );
}
