"use client"
import { useState } from 'react';
import Image from "next/image";
import screenshot from '/public/accests/reel-screenshot.png';
import './Component.css'
import {
    SfIconCheck,
    SfIconChevronLeft,
    SfAccordionItem,
} from '@storefront-ui/react';
import classNames from 'classnames';


const MobileDescription = () => {


    const [opened, setOpened] = useState([]);

    const descriptionFunction = () => {
        return (
            <>
                <div className='description-block'>
                    <p className='font-normal text-sm lg:text-base text-black'>
                        The 6.1-inch screen of the iPhone 13 Pro is equipped with Apple ProMotion, which makes scrolling or reading even more pleasant. The screen refreshes up to 120x per second when you are scrolling or gaming so that you always have the best screen experience. The iPhone 13 Pro now takes even sharper photos than its predecessors. The improved cameras and telephoto lens ensure that the photos are sharp, colorful and clear, even in lower light.
                    </p>
                    <p className='font-normal text-sm lg:text-base text-black'>
                        Also new to the iPhone 13 Pro is the cinematic mode that allows you to easily switch objects or people during or even after filming. The iPhone 13 Pro is equipped with the new A15 Bionic Chip and 6 GB of RAM. This makes this iPhone faster and more powerful. The battery has also been upgraded and lasts 1.5 hours longer than that of the iPhone 12 Pro.
                    </p>
                </div>
            </>
        )
    }

    const technicalFunction = () => {
        return (
            <>
                <div className="prose">
                    <table>
                        <thead>
                            <tr></tr>
                        </thead>
                        <tbody>
                            <tr className='p-4'>
                                <td className='td-white font-semibold text-sm lg:text-base'>Memory</td>
                                <td className='td-content font-normal text-sm lg:text-base'>128GB</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Processor</td>
                                <td className='td-content font-normal text-sm lg:text-base'>A15 Bionic</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Screen</td>
                                <td className='td-content font-normal text-sm lg:text-base'>6.1 inch</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Color</td>
                                <td className='td-content font-normal text-sm lg:text-base'>Goud</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Screen Type</td>
                                <td className='td-content font-normal text-sm lg:text-base'>OLEDCamera</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Cemera (front)</td>
                                <td className='td-content font-normal text-sm lg:text-base'>12MPCemera</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Cemera (back)</td>
                                <td className='td-content font-normal text-sm lg:text-base'>12MP</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Weight</td>
                                <td className='td-content font-normal text-sm lg:text-base'>203g</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Dimensions</td>
                                <td className='td-content font-normal text-sm lg:text-base'>146,7mm * 71,5mm * 7,65mm</td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Face ID</td>
                                <td className='td-content font-normal text-sm lg:text-base'><SfIconCheck className="" /></td>
                            </tr>
                            <tr>
                                <td className='td-white font-semibold text-sm lg:text-base'>Touched ID</td>
                                <td className='td-content font-normal text-sm lg:text-base'><SfIconCheck className="" /></td>
                            </tr>
                            <tr></tr>
                        </tbody>
                    </table>
                </div>
            </>
        )
    }

    const accordionItems = [
        {
            id: 'acc-2',
            summary: 'Description',
            details: descriptionFunction(),
        },
        {
            id: 'acc-1',
            summary: 'Technical specification',
            details: technicalFunction(),
        },

    ];

    const isOpen = (id) => opened.includes(id);

    const scoialVedioFunction = () =>{
        return (
            <Image
                src={screenshot}
                alt="screenshot"
                className="h-auto xl:h-[950px] w-100 border-radius-16"
            />
        )
    }

    const handleToggle = (id) => (open) => {
        if (open) {
            setOpened((current) => [...current, id]);
        } else {
            setOpened((current) => current.filter((item) => item !== id));
        }
    };

    return (
        <>
            <div className='container  hidden md:flex'>
                <div className='row'>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                        <div className='xl:pr-8 pr-2'>
                            <h5 className='lg:mb-[40px] mb-3 text-black font-semibold text-base lg:text-[22px] lg:leading-[28px]'>
                            Description
                            </h5>
                            {descriptionFunction()}
                            <h5 className='lg:mb-[40px] mb-3 text-black font-semibold text-base lg:text-[22px] lg:leading-[28px] mt-10'>
                            Technical specification
                            </h5>
                            {technicalFunction()}
                        </div>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12' >
                        <div className='xl:pl-8 pl-2'>
                            {scoialVedioFunction()}
                        </div>
                    </div>
                </div>
            </div>
            <div className='container md:hidden lg:hidden'>
                <div className='row'>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 mb-8'>
                        <div className=" rounded-md divide-y text-neutral-900 details-accord">
                            {accordionItems.map(({ id, summary, details }) => (
                                <SfAccordionItem
                                key={id}
                                summary={
                                <div className="flex justify-between py-3 main-color  active:neutral-100 main-color font-semibold text-base">
                                    <p className='mb-0'>{summary}</p>
                                    <SfIconChevronLeft
                                    className={classNames('text-neutral-500', {
                                        'rotate-90': isOpen(id),
                                        '-rotate-90': !isOpen(id),
                                    })}
                                    />
                                </div>
                                }
                                onToggle={handleToggle(id)}
                                open={isOpen(id)}
                            >
                                    {details}

                                </SfAccordionItem>
                            ))}
                        </div>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12' >
                       {scoialVedioFunction()}
                    </div>
                </div>
            </div>
        </>
    )
}

export default MobileDescription