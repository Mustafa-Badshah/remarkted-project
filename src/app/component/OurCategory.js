import React from 'react';
import Image from "next/image";
import { SfButton } from '@storefront-ui/react';
import iphone from '/public/accests/iphone.png';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const  OurCategory = () => {

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        prevArrow: false,
        nextArrow: false
    };

    let movileViewCategory = [
        {
            id: 1,
            title: 'Budget-Friendly iPhones',
            img: 'iPhone'
        },
        {
            id: 2,
            title: 'Affordable Mackbooks',
            img: 'iPhone'
        },
        {
            id: 3,
            title: 'Amazing iPads',
            img: 'iPhone'
        },
        {
            id: 4,
            title: 'Useful Services',
            img: 'iPhone'
        },
        {
            id: 5,
            title: 'Accessories',
            img: 'iPhone'
        }
    ]


    return (
        <>
            <div className='hidden md:block'>
                <div className='row justify-between items-center mb-8 flex'>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                        <div className='w-full xl:pr-10 pr-1'>
                            <div className='bg-white shadow rounded-3xl p-8 xl:pr-20 pr-4'>
                                <div className='row justify-between items-center'>
                                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 '>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>Budget-Friendly iPhones</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[135px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                        <div className='w-full xl:pl-10 pl-1 '>
                            <div className='bg-white shadow rounded-3xl p-8 xl:pr-20 pr-4'>
                                <div className='row justify-between'>
                                    <div className='col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>Affordable <br/> Macbooks</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[135px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='row justify-center items-center hidden lg:flex'>
                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 2xl:h-[auto] h-[200px]'>
                        <div className='w-full xl:pr-3 pr-0 h-100'>
                            <div className='bg-white shadow rounded-3xl p-7 xl:pr-10 pr-2 h-100'>
                                <div className='row justify-between items-center h-100'>
                                    <div className='col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 '>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>Amazing iPads</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[105px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 2xl:h-[auto] h-[200px]'>
                        <div className='w-full xl:px-3 px-0 h-100'>
                            <div className='bg-white shadow rounded-3xl p-7 xl:pr-10 pr-2 h-100'>
                                <div className='row justify-between items-center h-100'>
                                    <div className='col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 '>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>Useful Services</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[105px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 2xl:h-[auto] h-[200px] last-category-col'>
                        <div className='w-full xl:pl-3 pl-0 h-100'>
                            <div className='bg-white shadow rounded-3xl p-7 xl:pr-10 pr-2 h-100'>
                                <div className='row justify-between items-center h-100'>
                                    <div className='col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 '>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>Accessories</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[105px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className='md:hidden text-class shadow bg-white rounded-3xl'>
                <Slider {...settings}>
                    {movileViewCategory.map((data) => (
                        <div key={data.id}>
                            <div className=' p-7 pr-10'>
                                <div className='row justify-between items-center min-h-[200px]'>
                                    <div className='col-8'>
                                        <h3 className='text-[22px] leading-[28px] font-semibold text-black mb-10'>{data?.title}</h3>
                                        <SfButton className='border-radius-16 font-medium text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                                            View
                                        </SfButton>
                                    </div>
                                    <div className='col-4'>
                                        <Image
                                            src={iphone}
                                            alt="iphone"
                                            className="w-[105px]"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>

                    ))
                    }
                </Slider>
            </div>
        </>
    )
}

export default OurCategory