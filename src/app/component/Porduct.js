import { SfButton, SfLink, SfIconLocalShipping } from '@storefront-ui/react';
import Image from "next/image";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import iphone from '/public/accests/image2.png';
import './Component.css'

export default function Product() {

    const liveUrl = 'http://localhost:3000/Views/Product'
    // const liveUrl = 'https://testfront.remarketed.com/Views/Product'

    var settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 1,responsive: [
            {
                breakpoint: 1350,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: false,
                  centerMode : false
                }
            },
            {
                breakpoint: 1200,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: false,
                  centerMode : false
                }
            },
            {
                breakpoint: 1100,
                settings: {
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  infinite: true,
                  dots: false,
                  centerMode : false
                }
            },
          {
            breakpoint: 990,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: false,
              centerMode : false
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 1,
              dots: true,
              centerMode : true,
              arrows : false,
            }
          },
          {
            breakpoint: 575,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true,
              centerMode : false,
              arrows : false,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true,
              centerMode : false,
              arrows : false,
            }
          }
        ]
      };


    const productData = [
        {
            id: 1,
            productImage: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/sneakers.png',
            productHeading: 'iPhone 13 pro 128GB Black',
            productShipping: 'Next day delivery',
            productPrice: '$729,99',
            productDis: '$669.99'
        },
        {
            id: 2,
            productImage: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/sneakers.png',
            productHeading: 'iPhone 13 pro 128GB Black',
            productShipping: 'Next day delivery',
            productPrice: '$729,99',
            productDis: '$669.99'
        },
        {
            id: 3,
            productImage: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/sneakers.png',
            productHeading: 'iPhone 13 pro 128GB Black',
            productShipping: 'Next day delivery',
            productPrice: '$729,99',
            productDis: '$669.99'
        },
        {
            id: 4,
            productImage: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/sneakers.png',
            productHeading: 'iPhone 13 pro 128GB Black',
            productShipping: 'Next day delivery',
            productPrice: '$729,99',
            productDis: '$669.99'
        }
    ]

    return (
        <>
            <div className='hidden lg:flex products-wrapper'>
                {productData.map((data) => (
                    // {console.log("--data", data)}
                    <div className='flex justify-center px-3 min-w-[270px] w-[25%] card-margin-top product-card'>
                        <div>
                            <div className="max-w-[300px] bgcolor-white card-box-shadow py-[32px]">
                                <div className="relative px-3 pb-4">
                                    <SfLink href={`${liveUrl}`} className="flex justify-center h-[160px]">
                                        <img
                                            src={iphone.src}
                                            alt="Great product"
                                            className="object-cover h-100 rounded-md w-fit"
                                            width="300"
                                            height="300"
                                        />
                                    </SfLink>
                                    <div className='sub-percent font-semibold text-sm text-white absolute top-[-10px] right-4'>
                                        -25%
                                    </div>
                                </div>
                                <div className="px-4 border-neutral-200">
                                    <SfLink href={`${liveUrl}`} variant="secondary" className="no-underline product-heading text-base lg:text-xl font-semibold hover:text-black mb-2 block">
                                        {data?.productHeading}
                                    </SfLink>
                                    <p className="block delivery-text color-sub2 text-sm lg:text-base font-semibold mb-3">
                                        <SfIconLocalShipping /> {data?.productShipping}
                                    </p>
                                    <div className='card-div-btn'>
                                        <div className='width-50'>
                                            <span className=" typography-text-lg text-sm lg:text-base font-normal" style={{ textDecoration: 'line-through' }}>{data?.productPrice}  </span>
                                            <span className=" typography-text-lg text-sm lg:text-base font-normal">new</span>
                                            <span className="block pb-1 font-bold typography-text-lg price-font">{data?.productDis}</span>
                                        </div>
                                        <div className='width-50'>
                                            <SfButton size="md" className='mt-2 bgcolor-sub1 m-font-femily h-[48px] w-[110px] main-color text-base font-semibold border-radius-16 bgmain-hover-color' variant="secondary-800">
                                                Choose
                                            </SfButton>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                ))
                }

            </div>

            <div className='lg:hidden text-class'>
                <Slider {...settings}>
                    {productData.map((data) => (
                        <div key={data.id} className='px-3'>

                            <div className="max-w-[300px] bgcolor-white card-box-shadow py-[32px]">
                                <div className="relative px-3 pb-4">
                                    <SfLink href={`${liveUrl}`} className="flex justify-center h-[160px]">
                                        <img
                                            src={iphone.src}
                                            alt="Great product"
                                            className="object-cover h-100 rounded-md w-fit"
                                            width="300"
                                            height="300"
                                        />
                                    </SfLink>
                                    <div className='sub-percent font-semibold text-sm text-white absolute top-[-10px] right-4'>
                                        -25%
                                    </div>
                                </div>
                                <div className="px-4 border-neutral-200">
                                    <SfLink href={`${liveUrl}`} variant="secondary" className="no-underline product-heading text-base lg:text-xl font-semibold hover:text-black mb-2 block">
                                        {data?.productHeading}
                                    </SfLink>
                                    <p className="block delivery-text color-sub2 text-sm lg:text-base font-semibold mb-3">
                                        <SfIconLocalShipping /> {data?.productShipping}
                                    </p>
                                    <div className='card-div-btn'>
                                        <div className='width-50'>
                                            <span className=" typography-text-lg text-sm lg:text-base font-normal" style={{ textDecoration: 'line-through' }}>{data?.productPrice}  </span>
                                            <span className=" typography-text-lg text-sm lg:text-base font-normal">new</span>
                                            <span className="block pb-1 font-bold typography-text-lg md:text-lg text-md">{data?.productDis}</span>
                                        </div>
                                        <div className='width-50'>
                                            <SfButton size="md" className='mt-2 bgcolor-sub1 m-font-femily h-[48px] w-[110px] main-color text-base font-semibold border-radius-16 bgmain-hover-color' variant="secondary-800">
                                                Choose
                                            </SfButton>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    ))
                    }
                </Slider>
            </div>
        </>
    );
}
