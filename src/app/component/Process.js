import { SfButton } from '@storefront-ui/react';
import Image from "next/image";
import ProcessImg from '/public/accests/Image.png';
import StoreImg from '/public/accests/NewImg.png';
import './Component.css'

const cardDetails = [
    {
        image: ProcessImg,
        title: 'Check',
        description:
            'All our iPhone are checked by our technicians upon arrival.',
        button: 'Read more',
    },
    {
        image: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/card-2.png',
        title: 'Renovate',
        description:
            'All our iPhone are checked by our technicians upon arrival .',
        button: 'Read more',
    },
    {
        image: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/card-1.png',
        title: 'Clean',
        description:
            'All our iPhone are checked by our technicians upon arrival.',
        button: 'Read more',
    },
    {
        image: 'https://storage.googleapis.com/sfui_docs_artifacts_bucket_public/production/card-1.png',
        title: 'iPhone as new',
        description:
            'All our iPhone are checked by our technicians upon arrival.',
        button: 'Read more',
    },
];

export default function Product() {

    return (
        <>
            <div className='main-process-div'>
                <div className='container'>
                    <div className='row'>
                        {cardDetails.map(({ image, title, description }) => (
                            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                                <div
                                    key={title}
                                    className="flex flex-col  rounded-md hover:shadow-xl process-p"
                                >
                                    <a
                                        className="absolute inset-0 z-1 focus-visible:outline focus-visible:outline-offset focus-visible:rounded-md"
                                        href="#"
                                        aria-label={title}
                                    />
                                    <Image src={ProcessImg} alt={title} className="" width={130} height={140} />
                                    <div className="flex flex-col items-start p-3 grow process-p">
                                        <p className="process-title font-semibold text-base lg:text-xl">{title}</p>
                                        <p className=" process-des text-sm lg:text-base">{description}</p>

                                    </div>
                                </div>
                            </div>
                        ))}

                    </div>
                    <div className='process-btn-div'>
                        <SfButton className='process-btn hover:bg-[#013049] text-white' variant="secondary-800">
                            View All Process
                        </SfButton>
                    </div>
                </div>
            </div>

            <div className='main-visit-store'>
                <div className='container'>
                    <div className='row'>
                        <div className='col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 md:m-0' style={{ padding: '0px' }}>
                            <div>
                                <Image src={StoreImg} className='visit-img lg:rounded-s-2xl' />
                            </div>
                        </div>
                        <div className='col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12 lg:rounded-r-2xl bgmain-color visit-store-col ' >
                            <div className='visit-store sm:pt-[40px] xs:pt-[40px] sm:pb-[40px] xs:pb-[40px]'>
                                <div className='visit-content'>
                                    <h1 className='visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>Visit our stores</h1>
                                    <p className='visit-p text-sm lg:text-sm xl:text-base'>To help our customers as best as possible, you can go to the Remarketed store in Groningen with all your question about your iPhone. Or one of our service points via partner Fixers.</p>
                                    <SfButton className='visit-store-btn text-sm lg:text-base text-[#013049] h-12 lg:w-44' variant="secondary-800">
                                        View our stores
                                    </SfButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
