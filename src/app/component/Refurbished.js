import Image from "next/image";
import ProductImage from '/public/accests/image2.png';
import RlogoImage from '/public/accests/r-logo.png';
import { SfIconCheck } from '@storefront-ui/react';
import './Component.css'

const cardDetails = [
    {
      description: 'iPhone 13 Pro 128GB Black.',
    },
    {
      description: 'iPhone 13 Pro 128GB Black.',
    },
    {
        description: 'iPhone 13 Pro 128GB Black.',
    },
    {
      description: 'iPhone 13 Pro 128GB Black.',
    },
    {
       description: 'iPhone 13 Pro 128GB Black.',
    },
    {
        description: 'iPhone 13 Pro 128GB Black.',
    },
    {
       description: 'iPhone 13 Pro 128GB Black.',
    },
    {
       description: 'iPhone 13 Pro 128GB Black.',
    },
    {
       description: 'iPhone 13 Pro 128GB Black.',
    },
    {
        description: 'iPhone 13 Pro 128GB Black.',
     },
     {
        description: 'iPhone 13 Pro 128GB Black.',
     },
     {
        description: 'iPhone 13 Pro 128GB Black.',
     },
     
  ]

export default function RefurbishedPhones() {
    return (
        <>
            <h2 className='typography-display-2 card-heading text-xl lg:text-3xl font-semibold'>
                All refurbished phones
            </h2>
            <div className="flex gap-4 lg:gap-6 ">
                <div className='row'>
                    {cardDetails.map(({ description }) => (
                        <div className="flex  col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 max-w-[280px] bg-white refurbished-card border-radius-16 card-margin-top">
                            <div className=" flex flex-row">
                                <Image src={ProductImage}  className='refurbished-img' />
                                {/* <div> */}
                                    <p className='ml-3.5 m-font-femily text-base font-semibold'> {description}</p>
                                {/* </div> */}
                            </div>
                        </div>
                    ))}

                </div>
            </div>
        </>
    )
}