import Image from "next/image";
import RlogoImage from '/public/accests/r-logo.png';
import { SfButton, SfIconCheck } from '@storefront-ui/react';
import './Component.css'


export default function RefurbishedBanner() {
    return (
        <>
            <div className="refurbihed-banner bgmain-color">
                <div className="container">
                    <div className="row items-center ">
                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div className=" quility-block banner-block flex gap-6 md:gap-16 items-center md:justify-between">
                                <div className="md:order-1 order-2">
                                    <h2 className='visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>The quility of new.</h2>
                                    <h2 className='visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px]'>The price of Refurbished.</h2>
                                    <SfButton className='bg-transparent w-fit h-[48px] text-white font-medium text-sm lg:text-lg border-radius-16 border-white border mt-4' variant="secondary-800"> Learn More</SfButton>
                                </div>
                                <Image src={RlogoImage} width={128} className="refurbished-banner-img order-1 md:order-2" />
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div className="banner-block md:mt-[20px]">
                              <p className="flex points font-medium text-white text-sm xl:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px] " /><span> Battery lasts a day at minimum</span></p>
                              <p  className="flex points  font-medium text-white text-sm xl:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px]" /><span> Safe and reliable devices from our own stock</span></p>
                              <p className="flex points font-medium text-white text-sm xl:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px] " /><span> Always a working device. Displeased? Get your money back!</span></p>
                              <p className="flex points  font-medium text-white text-sm xl:text-base mb-[20px]"><SfIconCheck className="refurbished-b-check main-color mr-[12px]" /><span> Customer service with a human touch first</span></p>
                              <p className="flex points  font-medium text-white text-sm xl:text-base"><SfIconCheck className="refurbished-b-check main-color mr-[12px]" /><span> Lots of extras as a Remarketed Family member!</span></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}