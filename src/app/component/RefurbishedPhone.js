import React from 'react';
import Image from "next/image";
import ChargerImg from '/public/accests/chargerImg.png';
import {
    SfLink,
} from '@storefront-ui/react';

const RefurbishedPhone = () => {
    return (
        <div className="container">
            <h2 className='card-heading font-semibold text-[22px] mb-0 leading-[28px] lg:text-[32px] lg:leading-[38px]'>
                Free with every iPhone
            </h2>
            <div className='d-flex content-between items-center free-cards-wrapper'>
                <div className='w-[25%] px-3 lg:min-w-[304px] min-w-[212px]  card-margin-top'>
                    <div className="bgcolor-white card-box-shadow plus-icon">
                        <div className="relative">
                            <SfLink href="#" className="flex justify-content-center height-img">
                                <Image
                                    src={ChargerImg}
                                    className='free-Iphone-img'
                                />
                            </SfLink>
                        </div>

                    </div>
                </div>
                <div className='w-[25%] px-3 lg:min-w-[320px] min-w-[230px]  card-margin-top'>
                    <div className="bgcolor-white card-box-shadow plus-icon">
                        <div className="relative">
                            <SfLink href="#" className="flex justify-content-center height-img">
                                <Image
                                    src={ChargerImg}
                                    className='free-Iphone-img'
                                />
                            </SfLink>
                        </div>

                    </div>
                </div>
                <div className='w-[25%] px-3 lg:min-w-[320px] min-w-[230px]  card-margin-top'>
                    <div className="bgcolor-white card-box-shadow plus-icon">
                        <div className="relative">
                            <SfLink href="#" className="flex justify-content-center height-img">
                                <Image
                                    src={ChargerImg}
                                    className='free-Iphone-img'
                                />
                            </SfLink>
                        </div>

                    </div>
                </div>
                <div className='w-[25%] px-3 lg:min-w-[304px] min-w-[212px]  card-margin-top'>
                    <div className="bgcolor-white card-box-shadow">
                        <div className="relative">
                            <SfLink href="#" className="flex justify-content-center height-img">
                                <Image
                                    src={ChargerImg}
                                    className='free-Iphone-img'
                                />
                            </SfLink>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )
}

export default RefurbishedPhone