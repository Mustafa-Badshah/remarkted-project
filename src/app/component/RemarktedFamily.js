import { SfButton } from '@storefront-ui/react';
import Image from "next/image";
import logo from '/public/accests/RemarketedLogo.png';
import StoreImg from '/public/accests/NewImg.png';
import bannerImg from '/public/accests/banner-img.jpg';
import './Component.css'

export default function RemarketedFamily() {

    return (
        <>
           <div className='row 2xl:h-[480px] xl:h-[440px] lg:h-[420px] lg:rounded-none rounded-3xl overflow-hidden relative mx-0 remarketed-row'>
              <div className='col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12 md:m-0 h-full visit-img' style={{ padding: '0px' }}>
                    <Image src={bannerImg} className='lg:rounded-s-2xl w-100 h-full' />
              </div>
              <div className='col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 lg:rounded-r-2xl bgmain-color visit-store-col h-full' >
                    <div className='visit-content'>
                        <Image
                            src={logo}
                            alt="logo"
                            className="h-[auto] mb-3 remarketed-icon"
                            width={90}
                        />
                        <h2 className='visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] hidden md:block'>Remarketed Family</h2>
                        <h2 className='visit-heading font-semibold text-[22px] leading-[28px] lg:text-[32px] lg:leading-[38px] md:hidden block'>Become a member of Remarketed Family</h2>
                        <ul className='xl:my-4 my-3 list-disc'>
                            <li className='text-white font-light mb-0.5 md:text-base text-sm'>5% discount on every order</li>
                            <li className='text-white font-light mb-0.5 md:text-base text-sm'>3 years warranty (instead of 2)</li>
                            <li className='text-white font-light mb-0.5 md:text-base text-sm'>Exclusive deals</li>
                            <li className='text-white font-light mb-0.5 md:text-base text-sm'>Free screen protector with your first order and more...</li>
                        </ul>
                        <SfButton className='visit-store-btn font-semibold md:text-base text-sm text-[#013049] h-12 lg:w-50 bgmain-hover-color' variant="secondary-800">
                        Become a member
                        </SfButton>
                    </div>
              </div>
            </div>
        </>
    );
}
