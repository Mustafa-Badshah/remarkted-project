import { SfButton } from '@storefront-ui/react'
import React from 'react'

function Savings() {
  return (
    <div className='row bg-[#013049] items-center'>
        <div className='col-lg-7 col-xl-8 left-img h-100'>

        </div>
        <div className='col-lg-5 col-xl-4 bg-[#013049]'>
          <div className='lg:p-0 pr-3 pl-3 sm:p-8  pb-12 pt-2'>
            <h2 className=' typography-display-2 md:typography-display-1 xl:leading-[60.5px] md:leading-[40px] mt-2 mb-4 text-[24px] leading-[30px] md:text-[36px] xl:text-[54px] text-white font-semibold banner-header'>The circular economy is now.</h2>
            <SfButton className='visit-store-btn font-semibold text-sm lg:text-base text-[#013049] h-12 lg:w-50 bgmain-hover-color' variant="secondary-800">
                Explore more
            </SfButton>
          </div>
        </div>
    </div>
  )
}

export default Savings