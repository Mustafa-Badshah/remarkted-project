import React from 'react';
import Image from "next/image";
import screenshot from '/public/accests/reel-screenshot.png';
import Instagram from '/public/accests/instagram.svg';
import Facebook from '/public/accests/facebook.svg';
import Youtube from '/public/accests/youtube.svg';
import Tiktok from '/public/accests/tiktok.svg';

const SocialHandling = () => {
  return (
    <div className='row items-center lg:gap-16 social-handling-container'>
        <div className='col-xl-4 col-lg-5 col-md-6 col-sm-6 col-12 social-handling-col' >
            <Image
                src={screenshot}
                alt="screenshot"
                className="h-auto w-100 border-radius-16"
            />
        </div>
        <div className='col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 social-handling-col' >
            <div className='social-text'>
                <h2 className='typography-display-2 md:typography-display-1 xl:leading-[60.5px] lg:leading-[55px] mt-2 mb-4 md:leading-[48px] md:text-[40px] leading-[34px] text-[28px] lg:text-[52px] xl:text-[54px] text-black font-semibold banner-header'>Get the most out of your devices</h2>
                <h3 className='text-[#2FAC8E] my-6 lg:my-9 lg:text-[28px] text-[22px] xl:text-[32px]'>Follow us for handy life-hacks</h3>
                <div className='social-links flex items-center gap-2 lg:gap-4'>
                    <a href='instagram.com' target='_blank'>
                        <Image src={Instagram} alt="social-icon" className="w-[46px] lg:w-[64px] xl:w-88 border-radius-16" />
                    </a>
                    <a href='tiktok.com' target='_blank'>
                        <Image src={Tiktok} alt="social-icon" className="w-[46px] lg:w-[64px] xl:w-88 border-radius-16" />
                    </a>
                    <a href='youtube.com' target='_blank'>
                        <Image src={Youtube} alt="social-icon" className="w-[46px] lg:w-[64px] xl:w-88 border-radius-16" />
                    </a>
                    <a href='facebook.com' target='_blank'>
                        <Image src={Facebook} alt="social-icon" className="w-[46px] lg:w-[64px] xl:w-88 border-radius-16" />
                    </a>
                </div>
            </div>
        </div>
    </div>
  )
}

export default SocialHandling