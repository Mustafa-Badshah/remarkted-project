import { SfButton } from '@storefront-ui/react';
import Image from "next/image";
import logo from '/public/accests/RemarketedLogo.png';
import StoreImg from '/public/accests/NewImg.png';
import bannerImg from '/public/accests/banner-img.jpg';
import './Component.css'

export default function VisitStore() {

    return (
        <>
           <div className='row '>
              <div className='col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 md:m-0' style={{ padding: '0px' }}>
                  <div>
                      <Image src={bannerImg} className='visit-img lg:rounded-s-2xl w-100' />
                  </div>
              </div>
              <div className='col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12 lg:rounded-r-2xl bgmain-color visit-store-col ' >
                  <div className='visit-store sm:pt-[40px] xs:pt-[40px] sm:pb-[40px] xs:pb-[40px]'>
                      <div className='visit-content'>
                        <Image
                                src={logo}
                                alt="logo"
                                className="h-full mb-3"
                                width={90}
                            />
                          <h2 className='visit-heading font-semibold text-xl lg:text-3xl'>Remarketed Family</h2>
                          <ul className='my-4 list-disc'>
                            <li className='text-white font-light mb-0.5'>5% discount on every order</li>
                            <li className='text-white font-light mb-0.5'>3 years warranty (instead of 2)</li>
                            <li className='text-white font-light mb-0.5'>Exclusive deals</li>
                            <li className='text-white font-light mb-0.5'>Free screen protector with your first order and more...</li>
                          </ul>
                          <SfButton className='visit-store-btn font-semibold text-sm lg:text-base text-[#013049] h-12 lg:w-50' variant="secondary-800">
                            Become a member
                          </SfButton>
                      </div>
                  </div>
              </div>
            </div>
        </>
    );
}
