import React from 'react';
import Image from "next/image";
import { SfButton } from '@storefront-ui/react';
import StoreImg from '/public/accests/StoreImg.png';

const VisitStore= () => {
    return (
        <div className='row items-center'>
            <div className='col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12 visit-text' >
                <div className='social-text mb-[25px]'>
                    <h2 className='typography-display-2 md:typography-display-1 md:leading-[67.5px] font-semibold mt-2 text-[28px] lg:text-[54px] text-black banner-header'>Visit our store</h2>
                    <div className='text-sm lg:text-base font-normal mt-4 text-black'>
                        <p>Unsure which device to choose? Questions about switching from Android to iOS? </p>
                        <p>We are happy to help. Visit our Remarketed shop in Groningen.</p>

                        <SfButton className='mt-3 border-radius-16 font-semibold text-sm lg:text-base text-white bgmain-color h-12 lg:w-50 bgsubbtn-hover' variant="secondary-800">
                            View Store
                        </SfButton>
                    </div>
                </div>
            </div>
            <div className='col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12 visit-image' >
                <Image
                    src={StoreImg}
                    alt="StoreImg"
                    className="h-auto w-100 border-radius-16"
                />
            </div>
        </div>
    )
}

export default VisitStore