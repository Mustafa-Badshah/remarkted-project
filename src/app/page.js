"use client"
import Dashboard from "./Views/Dashboard/page";
import { gql, useQuery } from '@apollo/client'
import GetItems from './GraphqlApi/mutation/createMutation.graphql'


export default function Home() {

  const {data, loading, error} = useQuery(GetItems)
  // console.log("--data", data, loading, error)

  return (
    <>
    <Dashboard />
    </>
  );
}
